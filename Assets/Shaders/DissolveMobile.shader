﻿Shader "VFX/Dissolve/DissolveMobile" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_BumpMap("Normal Map", 2D) = "bump" {}
		_NoiseTex("NoiseTex", 2D) = "white" {}
		_RampTex("RampTex", 2D) = "white" {}
		_Fade("DissolveAmount", Range(0, 1)) = 0.0
		_Cutoff("Alpha cutoff", Range(0, 1)) = 0.263
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM


		//#pragma surface surf Standard alphatest:_Cutoff fullforwardshadows
		#pragma surface surf Lambert alphatest:_Cutoff noforwardadd 

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0


		sampler2D _MainTex;
		sampler2D _BumpMap;


		//DissolvePart
		sampler2D _NoiseTex;
		float4 _NoiseTex_ST;
		sampler2D _RampTex;
		float4 _RampTex_ST;

		fixed _Fade;
		fixed4 _Color;
		fixed3 _GlowColor;


		struct Input {
			float2 uv_MainTex;
			float2 uv_BumpMap;
		};

		


			void surf(Input IN, inout SurfaceOutput o) {

			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;

			//Clipping
			float NoiseTex_var = tex2D(_NoiseTex, TRANSFORM_TEX(IN.uv_MainTex, _NoiseTex)).a;
			float clip_val = saturate(((((1 - _Fade) * 1.2 + -0.6) + NoiseTex_var) * 10.0 + -5.0));

			////// Emission
			float2 emission_uv = float2((1.0 - clip_val), 0.0);
			float3 ramp = tex2D(_RampTex, TRANSFORM_TEX(emission_uv, _RampTex)).rgb;

			fixed4 tex = tex2D(_MainTex, IN.uv_MainTex)*_Color;
			o.Emission = tex.rgb * _GlowColor + ramp;

			o.Alpha = clip_val;
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
		}
		ENDCG
	}
	FallBack "Diffuse"
}
