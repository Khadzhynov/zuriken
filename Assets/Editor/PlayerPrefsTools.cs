﻿using UnityEditor;
using UnityEngine;

public static class PlayerPrefsTools {

    [MenuItem("Tools/Clear Player Prefs")]
    public static void ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
}
