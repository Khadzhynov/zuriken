﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MenuZombieIitializer : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    [MenuItem("Zombie/SetZombie")]
    public static void SetZombie() {
        foreach (var zombie in Selection.gameObjects) {
            foreach (Transform bodyPart in zombie.transform) {
                bodyPart.tag = "Zombie";
                SetChildren(bodyPart);
            }
        }
    }

    private static void SetChildren(Transform bodyPart) {
        if (bodyPart.gameObject.name == "Head")
        {
            bodyPart.gameObject.tag = "Head";
        }
        else {
            bodyPart.gameObject.tag = "Zombie";
        }
        foreach (Transform part in bodyPart) {
            if (part.gameObject.name == "Head")
            {
                part.gameObject.tag = "Head";
            }
            else {
                part.gameObject.tag = "Zombie";
            }
            SetChildren(part);
        }
    }
}
