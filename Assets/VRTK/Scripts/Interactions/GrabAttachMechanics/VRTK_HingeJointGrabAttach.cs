﻿// Fixed Joint Grab Attach|GrabAttachMechanics|50040
namespace VRTK.GrabAttachMechanics
{
    using UnityEngine;

    /// <summary>
    /// The Hinge Joint Grab Attach script is used to create a simple Fixed Joint connection between the object and the grabbing object.
    /// </summary>
    [AddComponentMenu("VRTK/Scripts/Interactions/Grab Attach Mechanics/VRTK_FixedJointGrabAttach")]
    public class VRTK_HingeJointGrabAttach : VRTK_BaseJointGrabAttach
    {
        [Tooltip("Maximum force the joint can withstand before breaking. Infinity means unbreakable.")]
        public float breakForce = 1500f;

        public Vector3 axis = Vector3.right;

        public Vector3 anchorOffset = Vector3.zero;

        public Vector3 connectedAnchorOffset = Vector3.zero;


        protected override void CreateJoint(GameObject obj)
        {
            givenJoint = obj.AddComponent<HingeJoint>();
            givenJoint.breakForce = (grabbedObjectScript.IsDroppable() ? breakForce : Mathf.Infinity);
            base.CreateJoint(obj);

            givenJoint.anchor = rightSnapHandle.localPosition + anchorOffset;
            givenJoint.connectedAnchor = connectedAnchorOffset;

            givenJoint.axis = axis;
        }


    }
}