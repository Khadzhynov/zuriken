﻿#define VIVE
// LocomotionSimpleAgent.cs
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
public class LocomotionSimpleAgent : MonoBehaviour
{
    public float Speed;

    private Animator _anim;
    private NavMeshAgent _agent;
    private Vector2 _smoothDeltaPosition = Vector2.zero;
    private Vector2 _velocity = Vector2.zero;

#if VIVE
    private Transform _target = null;

    public void SetTargetTransform(Transform t)
    {
        _target = t;
    }
#endif


    void Awake()
    {
        _anim = GetComponent<Animator>();
        _agent = GetComponent<NavMeshAgent>();
        // Don’t update position automatically
        _agent.updatePosition = false;
    }

    void Update()
    {

#if VIVE
        if (_target != null)
        {
            _agent.SetDestination(_target.position);
        }
#endif

        var worldDeltaPosition = _agent.nextPosition - transform.position;

        // Map 'worldDeltaPosition' to local space
        float dx = Vector3.Dot(transform.right, worldDeltaPosition);
        float dy = Vector3.Dot(transform.forward, worldDeltaPosition);
        Vector2 deltaPosition = new Vector2(dx, dy);

        // Low-pass filter the deltaMove
        float smooth = Mathf.Min(1.0f, Time.deltaTime / 0.15f);
        _smoothDeltaPosition = Vector2.Lerp(_smoothDeltaPosition, deltaPosition, smooth);

        // Update velocity if time advances
        if (Time.deltaTime > 1e-5f)
            _velocity = _smoothDeltaPosition / Time.deltaTime;

/*        //bool shouldMove = _velocity.magnitude > 0.5f && _agent.remainingDistance > _agent.radius;
        Speed = 0.01f; //_velocity.magnitude/100f;
        */
        // Update animation parameters
        _anim.SetFloat("Speed", Speed);

        GetComponent<LookAt>().lookAtTargetPosition = _agent.steeringTarget + transform.forward;

        // Pull character towards agent
        /*if (worldDeltaPosition.magnitude > _agent.radius)
            transform.position = _agent.nextPosition - 0.9f * worldDeltaPosition;*/

        // Pull agent towards character
        if (worldDeltaPosition.magnitude > _agent.radius)
            _agent.nextPosition = transform.position + 0.9f * worldDeltaPosition;
    }

    void OnAnimatorMove()
    {
        // Update position based on animation movement using navigation surface height
        Vector3 position = _anim.rootPosition;
        position.y = _agent.nextPosition.y;
        transform.position = position;
    }
}