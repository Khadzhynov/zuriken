﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class VRTK_Listener : MonoBehaviour
{

    public static event ControllerInteractionEventHandler OnTriggerPressed;
    public static event ControllerInteractionEventHandler OnTriggerReleased;

    public static event ControllerInteractionEventHandler OnTouchpadPressed;
    public static event ControllerInteractionEventHandler OnTouchpadReleased;

    public static event ControllerInteractionEventHandler OnGripPressed;
    public static event ControllerInteractionEventHandler OnGripReleased;


    private void Start()
    {
        var events = GetComponent<VRTK_ControllerEvents>();

        if (events == null)
        {
            VRTK_Logger.Error(VRTK_Logger.GetCommonMessage(VRTK_Logger.CommonMessageKeys.REQUIRED_COMPONENT_MISSING_FROM_GAMEOBJECT, "VRTK_ControllerEvents_ListenerExample", "VRTK_ControllerEvents", "the same"));
            return;
        }

        events.TriggerPressed += OnTriggerPressedHandler;
        events.TriggerReleased += OnTriggerReleasedHandler;
        events.TouchpadPressed += OnTouchpadPressedHandler;
        events.TouchpadReleased += OnTouchpadReleasedHandler;
        events.TouchpadPressed += OnGripPressedHandler;
        events.TouchpadReleased += OnGripReleasedHandler;
    }

    private void OnDestroy()
    {
        var events = GetComponent<VRTK_ControllerEvents>();

        if (events != null)
        {
            events.TriggerPressed -= OnTriggerPressedHandler;
            events.TriggerReleased -= OnTriggerReleasedHandler;
            events.TouchpadPressed -= OnTouchpadPressedHandler;
            events.TouchpadReleased -= OnTouchpadReleasedHandler;
            events.TouchpadPressed -= OnGripPressedHandler;
            events.TouchpadReleased -= OnGripReleasedHandler;
        }
    }

    private void OnTriggerPressedHandler(object sender, ControllerInteractionEventArgs e)
    {
        if (OnTriggerPressed != null)
            OnTriggerPressed(sender, e);

    }

    private void OnTriggerReleasedHandler(object sender, ControllerInteractionEventArgs e)
    {
        if (OnTriggerReleased != null)
            OnTriggerReleased(sender, e);
    }

    private void OnTouchpadPressedHandler(object sender, ControllerInteractionEventArgs e)
    {
        if (OnTouchpadPressed != null)
            OnTouchpadPressed(sender, e);
    }

    private void OnTouchpadReleasedHandler(object sender, ControllerInteractionEventArgs e)
    {
        if (OnTouchpadReleased != null)
            OnTouchpadReleased(sender, e);
    }

    private void OnGripPressedHandler(object sender, ControllerInteractionEventArgs e)
    {
        if (OnTouchpadPressed != null)
            OnGripPressed(sender, e);
    }

    private void OnGripReleasedHandler(object sender, ControllerInteractionEventArgs e)
    {
        if (OnTouchpadReleased != null)
            OnGripReleased(sender, e);
    }
}
