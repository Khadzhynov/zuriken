﻿#define VIVE

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using VRTK;

/// <summary>
/// This is custom pointer for VRTK. Default VRTK`s pointer is binded to physics, so it does not works when time scale is 0.
/// This one is timescale-independent.
/// </summary>
public class VIVE_CustomUIPointer : MonoBehaviour
{
    [SerializeField]
    private LineRenderer _lineRenderer;

    //[SerializeField]
    //private VRTK_ControllerEvents _events;

    [SerializeField]
    private bool _DefaultPointer;

    [SerializeField]
    private VRTK_ControllerEvents.ButtonAlias _togglePointerButton = VRTK_ControllerEvents.ButtonAlias.TriggerPress;
    [SerializeField]
    private VRTK_ControllerEvents.ButtonAlias _selectButton = VRTK_ControllerEvents.ButtonAlias.TouchpadPress;



    private static VIVE_CustomUIPointer _activePointer;

    public static event System.EventHandler _PointerButtonPressed;

    private void Start()
    {
        /*
        if (_events == null)
        {
            _events = gameObject.GetComponent<VRTK_ControllerEvents>();
            if (_events == null)
            {
                _events = gameObject.AddComponent<VRTK_ControllerEvents>();
            }
        }
        */

        if (_lineRenderer == null)
        {
            _lineRenderer = gameObject.GetComponent<LineRenderer>();

            if (_lineRenderer == null)
            {
                _lineRenderer = gameObject.AddComponent<LineRenderer>();

                _lineRenderer.startColor = Color.red;
                _lineRenderer.endColor = Color.red;
                _lineRenderer.startWidth = 0.01f;
                _lineRenderer.endWidth = 0.01f;
            }
        }

        if (_lineRenderer.enabled)
        {
            _activePointer = this;
        }


        gameObject.GetComponent<VRTK_ControllerEvents>().SubscribeToButtonAliasEvent(_togglePointerButton, true, PointerToggle);

        gameObject.GetComponent<VRTK_ControllerEvents>().SubscribeToButtonAliasEvent(_selectButton, true, PointerSelect);

        _DefaultPointer = _lineRenderer.enabled;


        //_events.TouchpadPressed += PointerSelect;
        //_events.TriggerPressed += PointerToggle;

        _PointerButtonPressed += OnPointerButtonPressed;

#if VIVE
        GameController.OnGoToMenu += AutoEnablePointer;
        GameController.OnGoToGameplay += DisablePointer;
#endif

    }

    private void OnPointerButtonPressed(object sender, System.EventArgs e)
    {
        if (_lineRenderer.enabled)
        {
            if (_activePointer != this)
            {
                _lineRenderer.enabled = false;
            }
        }
    }

    private void OnDestroy()
    {
        //_events.TouchpadPressed -= PointerSelect;
        //_events.TriggerPressed -= PointerToggle;

        gameObject.GetComponent<VRTK_ControllerEvents>().UnsubscribeToButtonAliasEvent(_togglePointerButton, true, PointerToggle);

        gameObject.GetComponent<VRTK_ControllerEvents>().UnsubscribeToButtonAliasEvent(_selectButton, true, PointerSelect);

        _PointerButtonPressed -= OnPointerButtonPressed;

#if VIVE
        GameController.OnGoToMenu -= AutoEnablePointer;
        GameController.OnGoToGameplay -= DisablePointer;
#endif
    }


    private void PointerToggle(object sender, ControllerInteractionEventArgs e)
    {
        if (_activePointer == this)
        {
            if (_lineRenderer.enabled)
            {

                RaycastHit hit;

                if (Physics.Raycast(transform.position, transform.forward, out hit))
                {
                    Button button = hit.collider.gameObject.GetComponent<Button>();
                    if (button != null)
                    {
                        button.onClick.Invoke();
                    }
                }
            }
        }
    }

    private void PointerSelect(object sender, ControllerInteractionEventArgs e)
    {
        if (_lineRenderer.enabled)
        {
            DisablePointer(null, null);
        }
        else
        {
            EnablePointer();
        }

        if (_PointerButtonPressed != null)
        {
            _PointerButtonPressed(null, null);
        }
    }

    private void EnablePointer()
    {
        _activePointer = this;
        _lineRenderer.enabled = true;
    }

    private void DisablePointer(object sender, System.EventArgs e)
    {
        if (_activePointer == this)
        {
            _activePointer = null;
        }
        _lineRenderer.enabled = false;
    }

    private void AutoEnablePointer(object sender, System.EventArgs e)
    {
        if (_DefaultPointer || _lineRenderer.enabled == true )
        {
            EnablePointer();
        }
    }

    private void LateUpdate()
    {
        if (_activePointer == this)
        {
            if (_lineRenderer.enabled)
            {
                Vector3 endPoint = transform.position + transform.forward * 100;

                RaycastHit hit;

                if (Physics.Raycast(transform.position, transform.forward, out hit))
                {

                    Button button = hit.collider.gameObject.GetComponent<Button>();

                    if (button != null)
                    {
                        EventSystem.current.SetSelectedGameObject(button.gameObject);
                    }
                    else
                    {
                        EventSystem.current.SetSelectedGameObject(null);
                    }

                    endPoint = hit.point;
                }
                else
                {
                    EventSystem.current.SetSelectedGameObject(null);
                }

                _lineRenderer.SetPosition(0, transform.position);
                _lineRenderer.SetPosition(1, endPoint);
            }
        }
    }

  
}
