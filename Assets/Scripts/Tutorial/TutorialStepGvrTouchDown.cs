﻿//#define GVR
#define VIVE

using UnityEngine;

[RequireComponent(typeof(TutorialStep))]
public class TutorialStepGvrTouchDown : MonoBehaviour
{
    private TutorialStep _tutorialStep;
    private bool _started;
    private void Awake()
    {
        _tutorialStep = GetComponent<TutorialStep>();
        _tutorialStep.SetOnStartHandler(StartStep);
    }

    private void StartStep()
    {
        _started = true;

#if VIVE
        TrowableSpawper.OnShurikenSpawned += CompleteTutorial;
        TrowableSpawper.OnShurikenThrown += CompleteTutorial;

    }

    private void CompleteTutorial(object sender, System.EventArgs e)
    {
        TrowableSpawper.OnShurikenSpawned -= CompleteTutorial;
        TrowableSpawper.OnShurikenThrown -= CompleteTutorial;

        _tutorialStep.CompleteStep();
#endif
    }

    private void Update()
    {
        if (!_started) return;

#if UNITY_EDITOR && GVR
        if (GvrController.TouchDown)
        {
#endif

#if GVR
        if (GvrController.ClickButtonDown) {
#endif

#if UNITY_EDITOR && VIVE
        if (Input.GetMouseButtonDown(0))
        {
#elif VIVE
        if (false)
        {
#endif

                _tutorialStep.CompleteStep();
        }
    }
}