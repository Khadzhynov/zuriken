﻿using UnityEngine;

[RequireComponent(typeof(TutorialStep))]
public class TutorialStepHitEnemy : MonoBehaviour
{
    [SerializeField]
    private SpawnPoint _spawnPoint;

    private TutorialStep _tutorialStep;
    private Transform _playerTransform;
    private Transform _enemiesParent;

    private void Awake()
    {
        _tutorialStep = GetComponent<TutorialStep>();
        _tutorialStep.SetOnStartHandler(StartStep);
    }

    public void Init(Transform playerTransform, Transform enemiesParent)
    {
        _playerTransform = playerTransform;
        _enemiesParent = enemiesParent;
    }

    public void StartStep()
    {
        if (_spawnPoint != null)
        {
            var zombie = _spawnPoint.Spawn();
            zombie.transform.SetParent(_enemiesParent);
            zombie.transform.LookAt(_playerTransform);
            var locomotionSimpleAgent = zombie.GetComponent<LocomotionSimpleAgent>();
            locomotionSimpleAgent.enabled = false;
            var zombieAnimator = zombie.GetComponent<Animator>();
            zombieAnimator.SetFloat("Speed", 0f);
            var zombieCollision = zombie.GetComponent<ZombieCollision>();
            zombieCollision.Init(_tutorialStep.CompleteStep);
        }
    }
}