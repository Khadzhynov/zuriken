﻿using UnityEngine;

public class TutorialStepFactory : MonoBehaviour
{
    private readonly ShurikenThrower _shurikenThrower;
    private readonly Transform _playerTransform;
    private readonly Transform _enemiesParent;

    public TutorialStepFactory(ShurikenThrower shurikenThrower, Transform playerTransform, Transform enemiesParent)
    {
        _shurikenThrower = shurikenThrower;
        _playerTransform = playerTransform;
        _enemiesParent = enemiesParent;
    }

    public TutorialStep Create(TutorialStep tutorialStepPrefab)
    {
        var tutorialStep = Instantiate(tutorialStepPrefab);
        var tutorialStepTakeShuriken = tutorialStep.GetComponent<TutorialStepTakeShuriken>();
        if (tutorialStepTakeShuriken != null)
            tutorialStepTakeShuriken.Init(_shurikenThrower);

        var tutorialStepThrowShuriken = tutorialStep.GetComponent<TutorialStepThrowShuriken>();
        if (tutorialStepThrowShuriken != null)
            tutorialStepThrowShuriken.Init(_shurikenThrower);

        var tutorialStepKillEnemy = tutorialStep.GetComponent<TutorialStepHitEnemy>();
        if (tutorialStepKillEnemy != null)
            tutorialStepKillEnemy.Init(_playerTransform, _enemiesParent);

        var tutorialStepGvrTouchDown = tutorialStep.GetComponent<TutorialStepGvrTouchDown>();
        if (tutorialStepGvrTouchDown != null) { /* No init here */ }

        return tutorialStep;
    }
}
