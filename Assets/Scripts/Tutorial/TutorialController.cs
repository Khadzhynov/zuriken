﻿using System;
using UnityEngine;

public class TutorialController : MonoBehaviour
{
    private Config _config;
    private TutorialStepFactory _tutorialStepFactory;
    private Transform _enemiesParent;

    private int _currentStepIndex;
    private Action _onTutorialCompleted;

    private TutorialStep _currentStep;

    public void Init(Config config, TutorialStepFactory tutorialStepFactory, Transform enemiesParent)
    {
        _config = config;
        _tutorialStepFactory = tutorialStepFactory;
        _enemiesParent = enemiesParent;
    }

    public void SetTutorialCompletedHandler(Action handler)
    {
        _onTutorialCompleted = handler;
    }

    public void StartTutorial()
    {
        ResetTutorial();
        StartNextTask();
    }

    private void StartNextTask()
    {
        _currentStep = _tutorialStepFactory.Create(_config.StepPrefabs[_currentStepIndex]);
        _currentStep.transform.SetParent(transform, false);
        _currentStep.SetOnCompletedHandler(OnStepCompleted);
        _currentStep.StartStep();
    }

    private void OnStepCompleted()
    {
        DestroyStep();
        _currentStepIndex++;
        if (_currentStepIndex < _config.StepPrefabs.Length)
            StartNextTask();
        else
        {
            if (_onTutorialCompleted != null) _onTutorialCompleted();
        }
    }

    private void DestroyStep() {
        if (_currentStep != null)
            Destroy(_currentStep.gameObject);
    }

    public void ResetTutorial()
    {
        DestroyStep();
        _currentStepIndex = 0;
        foreach (Transform zombie in _enemiesParent)
        {
            GameObject.Destroy(zombie.gameObject);
        }
    }

    [Serializable]
    public class Config
    {
        public TutorialStep[] StepPrefabs;
    }
}
