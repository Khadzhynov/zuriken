﻿//#define GVR
#define VIVE

using UnityEngine;

[RequireComponent(typeof(TutorialStep))]
public class TutorialStepTakeShuriken : MonoBehaviour
{
    private TutorialStep _tutorialStep;

    private void Awake()
    {
        _tutorialStep = GetComponent<TutorialStep>();
        _tutorialStep.SetOnStartHandler(StartStep);
    }

    private ShurikenThrower _shurikenThrower;
    public void Init(ShurikenThrower shurikenThrower)
    {
        _shurikenThrower = shurikenThrower;
    }

    private void StartStep()
    {
#if GVR
        _shurikenThrower.OnShurikenCreated += ShurikenThrowerOnShurikenCreated;
#endif

#if VIVE
        TrowableSpawper.OnShurikenSpawned += VIVE_ShurikenSpawner_OnShurikenSpawned; 

    }

    private void VIVE_ShurikenSpawner_OnShurikenSpawned(object sender, System.EventArgs e)
    {
        ShurikenThrowerOnShurikenCreated();
#endif
    }


    private void ShurikenThrowerOnShurikenCreated()
    {
#if GVR
        if (_shurikenThrower != null)
            _shurikenThrower.OnShurikenCreated -= ShurikenThrowerOnShurikenCreated;
#endif

#if VIVE
        TrowableSpawper.OnShurikenSpawned -= VIVE_ShurikenSpawner_OnShurikenSpawned;
#endif

        if (_tutorialStep != null)
            _tutorialStep.CompleteStep();
    }
}
