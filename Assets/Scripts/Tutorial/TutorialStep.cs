﻿using System;
using System.Collections;
using UnityEngine;

public class TutorialStep : MonoBehaviour
{
    [SerializeField]
    private float _delayBeforeCompletion;

    private Action _onStartHandler;
    public void SetOnStartHandler(Action handler)
    {
        _onStartHandler = handler;
    }

    private Action _onCompletedHandler;
    public void SetOnCompletedHandler(Action handler)
    {
        _onCompletedHandler = handler;
    }

    public void StartStep()
    {
        if (_onStartHandler != null)
            _onStartHandler();
    }

    public void CompleteStep()
    {
        StartCoroutine(CompletingStep());
    }

    private IEnumerator CompletingStep()
    {
        yield return new WaitForSeconds(_delayBeforeCompletion);
        if (_onCompletedHandler != null)
            _onCompletedHandler();
    }
}
