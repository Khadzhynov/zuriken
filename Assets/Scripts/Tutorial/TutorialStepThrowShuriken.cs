﻿//#define GVR
#define VIVE

using UnityEngine;

[RequireComponent(typeof(TutorialStep))]
public class TutorialStepThrowShuriken : MonoBehaviour
{
    private TutorialStep _tutorialStep;

    private void Awake()
    {
        _tutorialStep = GetComponent<TutorialStep>();
        _tutorialStep.SetOnStartHandler(StartStep);
    }

    private ShurikenThrower _shurikenThrower;
    public void Init(ShurikenThrower shurikenThrower)
    {
        _shurikenThrower = shurikenThrower;
    }

    private void StartStep()
    {
#if GVR
        _shurikenThrower.OnShurikenThrown += ShurikenThrowerOnShurikenThrown;
#endif

#if VIVE
        TrowableSpawper.OnShurikenThrown += VIVE_ShurikenSpawner_OnShurikenThrown; 

    }

    private void VIVE_ShurikenSpawner_OnShurikenThrown(object sender, System.EventArgs e)
    {
        ShurikenThrowerOnShurikenThrown();
#endif
    }

    private void ShurikenThrowerOnShurikenThrown()
    {
#if GVR
        _shurikenThrower.OnShurikenThrown -= ShurikenThrowerOnShurikenThrown;
#endif

#if VIVE
        TrowableSpawper.OnShurikenThrown -= VIVE_ShurikenSpawner_OnShurikenThrown;
#endif
        _tutorialStep.CompleteStep();
    }
}
