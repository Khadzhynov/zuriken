﻿#define VIVE

using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour {

    [SerializeField]
    private Transform _spawnPointsParentTransform;

    private int _waveIndex = -1;
    private SpawnPoint[] _spawnPoints;
    private Config _config;
    private Transform _playerTransform;
    private Transform _enemiesRootTransform;

    private void Awake()
    {
        _spawnPoints = _spawnPointsParentTransform.GetComponentsInChildren<SpawnPoint>();
    }

    public void Init(Config config, Transform playerTransform, Transform enemiesRootTransform)
    {
        _config = config;
        _playerTransform = playerTransform;
        _enemiesRootTransform = enemiesRootTransform;
    }

    private void ResetSpawner() {
        _waveIndex = -1;
    }

    public void StartSpawn () {
        ResetSpawner();
	    StartCoroutine(Check());
	}

    public void StopSpawn() {
       StopAllCoroutines();
        foreach (Transform zombie in _enemiesRootTransform) {
            Object.Destroy(zombie.gameObject);
        }
    }

    private IEnumerator Check() {
        if (CanSpawn()) {
            if (_waveIndex < _config.EnemyWaves.Length - 1)
                _waveIndex ++;
            Debug.Log("Wave: " + _waveIndex + " started");
            StartCoroutine(SpawnZombie(_config.EnemyWaves[_waveIndex].EnemyCount,
                _config.EnemyWaves[_waveIndex].EnemySpawnTime,
                _config.EnemyWaves[_waveIndex].EnemySpeedMultiplier));
            yield return new WaitForSeconds(_config.EnemyWaves[_waveIndex].TimeBeforeNextWave);
            StartCoroutine(Check());
        }
        else {
            yield return new WaitForSeconds(2f);
            StartCoroutine(Check());
        }
    }

    private bool CanSpawn() {
        return _enemiesRootTransform.childCount < _config.MaxEnemiesInScene;
    }

    private IEnumerator SpawnZombie(int count, float spawnTime, float zombieSpeedMultiplier) {
        for (var i = 0; i < count; i++) {
            var randomDelay = Random.Range(0f, spawnTime);
            StartCoroutine(Spawn(randomDelay, zombieSpeedMultiplier));
        }
        yield return null;
    }

    private IEnumerator Spawn(float delay, float speedMultiplier) {
        yield return new WaitForSeconds(delay);
        var spawnPoint = _spawnPoints[Random.Range(0, _spawnPoints.Length - 1)];
        var enemy = spawnPoint.Spawn();
        enemy.transform.SetParent(_enemiesRootTransform, true);
        enemy.GetComponent<NavMeshAgent>().SetDestination(_playerTransform.position);
#if VIVE
        var enemyAI = enemy.GetComponent<LocomotionSimpleAgent>();
        if (enemyAI != null)
        {
            enemyAI.SetTargetTransform(_playerTransform);
        }
#endif

        float speed = enemy.GetComponent<LocomotionSimpleAgent>().Speed;
        /////////10% chance for zombie to have +50% speed
        int rand = Random.Range(0, 10);
        if (rand == 0) {
            speed *= 1.5f;
        }

        if (spawnPoint.IsEnemyLayingState) {
            enemy.GetComponent<Animator>().SetBool("Fall", true);
            enemy.GetComponent<LocomotionSimpleAgent>().Speed = speed*speedMultiplier*speedMultiplier;
        }
        else {
            enemy.GetComponent<LocomotionSimpleAgent>().Speed = speed * speedMultiplier * speedMultiplier*1.255f;
        }
    }

    [Serializable]
    public class Config
    {
        public int MaxEnemiesInScene;
        public EnemyWave[] EnemyWaves;

        [Serializable]
        public class EnemyWave
        {
            public int EnemyCount;
            public float EnemySpeedMultiplier;
            public float EnemySpawnTime;
            public float TimeBeforeNextWave;
        }
    }
}
