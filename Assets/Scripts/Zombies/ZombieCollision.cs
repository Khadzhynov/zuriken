﻿using System;
using System.Collections;
using UnityEngine.AI;
using UnityEngine;

public class ZombieCollision : MonoBehaviour
{

    [SerializeField] private GameObject _bloodParticles;
    [SerializeField] private AudioClip _hitSound;
    [SerializeField] private AudioClip _biteSound;

    [SerializeField] private float _maxHealth;
    private float _currentHealth;

    [SerializeField] private float _score;

    [SerializeField] private float _headScoreMultipler;
    [SerializeField] private float _headDamageMultipler;

    private Animator _animator;
    private GvrAudioSource _audioSource;

    private Action _onDeath;
    private bool _isDead;

    private bool _canEntomb;
    private Vector3 _startPosition;
    private Vector3 _endPosition;
    private float _fraction;


    void Start () {
        _animator = GetComponent<Animator>();
        _audioSource = GetComponent<GvrAudioSource>();

        _currentHealth = _maxHealth;
    }

    public void Init(Action onDeath)
    {
        _onDeath = onDeath;
    }

	void Update () {
	    if (_canEntomb) {
	        if (_fraction < 1) {
	            _fraction += Time.deltaTime*0.2f;
	            transform.localPosition = Vector3.Lerp(_startPosition, _endPosition, _fraction);
	        }
	        else {
	            Destroy(gameObject);
	        }
	    }
	}

    private void OnTriggerEnter(Collider other)
    {
        if (_isDead) return;

        if (other.gameObject.tag == "Player")
        {
            StartCoroutine(Attack());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (_isDead) return;

        if (other.gameObject.tag == "Player")
        {
            _animator.SetTrigger("Reset");
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (_isDead) return;

        if (collision.gameObject.layer == LayerMask.NameToLayer("Weapon"))
        {
            WeaponCollisionBase damager = collision.gameObject.GetComponent<WeaponCollisionBase>();

            if (damager != null)
            {
                float damage = damager.GetDamage(collision);
                if (damage > 0)
                {
                    

                    float scoreMultipler = 1;
                    float damageMultipler = 1;

                    foreach (ContactPoint contact in collision.contacts)
                    {
                        Instantiate(_bloodParticles, contact.point, Quaternion.identity);

                        if (contact.thisCollider.gameObject.tag == "Head" || contact.otherCollider.gameObject.tag == "Head")
                        {
                            scoreMultipler = _headScoreMultipler;
                            damageMultipler = _headDamageMultipler;
                        }
                    }

                    float healthBefore = _currentHealth;

                    _currentHealth -= (damage * damageMultipler);

                    //Debug.Log("Zombie " + gameObject.name + " damaged by " + collision.gameObject.name + " with damage: " + damage + " health was " + healthBefore + ", health now " + _currentHealth);

                    _animator.SetTrigger("Hit");

                    if (_currentHealth <= 0)
                    {
                        ScoreView.GetScore().AddScore( (int) (damager.GetScoreMultipler() * _score * scoreMultipler) );

                        _audioSource.loop = false;
                        _audioSource.clip = _hitSound;
                        _audioSource.Play();

                        float delay = 0;
                        
                        delay = _animator.GetBool("Fall") ? 1.3f : 3.3f;
                        _animator.SetBool("Fall", true);

                        DeactivateChildColliders(gameObject.transform);

                        StartCoroutine(Death(delay));

                        _isDead = true;

                        if (_onDeath != null) _onDeath();
                    }

                }

                damager.OnImpact();
            }
        }
        else
        {
            if (collision.gameObject.tag == "Player")
            {
                StartCoroutine(Attack());
            }
        }
    }

    public IEnumerator Attack() {
        if (UnityEngine.Random.Range(0, 100) < 50)
        {
            _animator.SetInteger("Attack", 3);
        }
        else
        {
            _animator.SetInteger("Attack", 2);
        }

        _audioSource.loop = false;
        _audioSource.clip = _biteSound;
        _audioSource.Play();

        
        yield return new WaitUntil( () =>
            _animator.GetCurrentAnimatorStateInfo(1).IsName("zombie_neck_bite") 
            ||
            _animator.GetCurrentAnimatorStateInfo(1).IsName("zombie_attack")
            );
        

        //yield return new WaitForSeconds(1);

        _animator.SetInteger("Attack", 0);

    }

    private void DeactivateChildColliders(Transform bodyPart) {
        if (bodyPart.gameObject.GetComponent<Collider>() != null) {
            bodyPart.gameObject.GetComponent<Collider>().enabled = false;
        }
        foreach (Transform part in bodyPart) {
            DeactivateChildColliders(part);
        }
    }

    private IEnumerator Death(float delay) {
        yield return new WaitForSeconds(delay);
        _animator.SetFloat("Speed", 0);
        GetComponent<LocomotionSimpleAgent>().enabled = false;
        GetComponent<NavMeshAgent>().enabled = false;
        yield return new WaitForSeconds(1.5f);
        _startPosition = transform.localPosition;
        _endPosition = transform.localPosition;
        _endPosition.y -= 1f;
        _canEntomb = true;
    }
}
