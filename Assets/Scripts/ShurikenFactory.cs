﻿using UnityEngine;

public class ShurikenFactory
{
    private readonly GameObject _shurikenPrefab;
    private readonly Score _score;

	// Use this for initialization
	public ShurikenFactory(GameObject shurikenPrefab, Score score)
	{
	    _shurikenPrefab = shurikenPrefab;
	    _score = score;
	}

    public GameObject Create()
    {
        Debug.Log("Shuriken create:");
        var shuriken = Object.Instantiate(_shurikenPrefab);
        var shurikenCollisionHandler = shuriken.GetComponent<SurikenCollision>();
        shurikenCollisionHandler.Init(_score);
        return shuriken;
    }
}
