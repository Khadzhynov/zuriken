﻿//#define GVR
#define VIVE

using UnityEngine;
using VRTK;

public interface IResetGameHandler
{
    void ResetGame();
}

public class GameController : IStartTutorialHandler, IStartGameHandler, IResumeGameHandler, IRestartGameHandler, IResetGameHandler, IGameOverHandler, ITickable
{
    private readonly MenuNavigationController _menuNavigationController;
    private readonly CameraController _cameraController;
    private readonly TutorialPopupFactory _tutorialPopupFactory;
    private readonly PausePopupFactory _pausePopupFactory;
    private readonly GameOverPopupFactory _gameOverPopupFactory;
    private readonly SinglePlayerController _singlePlayerController;
    private readonly TutorialController _tutorialController;

#if VIVE
    public static event System.EventHandler OnGoToMenu;
    public static event System.EventHandler OnGoToGameplay;
#endif

    public GameController(MenuNavigationController menuNavigationController, CameraController cameraController,
        TutorialPopupFactory tutorialPopupFactory, PausePopupFactory pausePopupFactory, GameOverPopupFactory gameOverPopupFactory,
        SinglePlayerController singlePlayerController, TutorialController tutorialController)
    {
        _menuNavigationController = menuNavigationController;
        _cameraController = cameraController;
        _tutorialPopupFactory = tutorialPopupFactory;
        _pausePopupFactory = pausePopupFactory;
        _gameOverPopupFactory = gameOverPopupFactory;
        _singlePlayerController = singlePlayerController;
        _tutorialController = tutorialController;
    }

    public void Start()
    {
        ToMenu(true);

        //if (!_settings.SkipTutorial)
            //_menuNavigationController.ShowPopup(_tutorialPopupFactory.Create().gameObject);

#if VIVE
        VIVE_CustomUIPointer._PointerButtonPressed += OnPointerEnabled;
    }

    private void OnPointerEnabled(object sender, System.EventArgs e)
    {
        ToggleMenu(null, default(ControllerInteractionEventArgs));
#endif
    }

    private void OnDestroy()
    {
        VIVE_CustomUIPointer._PointerButtonPressed -= OnPointerEnabled;
    }

    public void StartGame()
    {
        ToGameplay();
        _singlePlayerController.StartGame();
    }

    public void ResumeGame()
    {
        ToGameplay();
    }

    public void RestartGame()
    {
        StartGame();
    }

    public void StartTutorial()
    {
        ToGameplay();
        _tutorialController.StartTutorial();
    }

    private void ToMenu(bool initial = false)
    {
        if (_cameraController != null)
        {
            _cameraController.CameraMode = CameraMode.Menu;
        }
        _menuNavigationController.IsVisible = true;

#if !UNITY_EDITOR || GVR
        Time.timeScale = 0;
#endif

#if VIVE
        if (initial)
        {
            VIVE_Teleporter.StartMenu();
        }
        else
        {
            Vector3 position = VRTK_DeviceFinder.HeadsetCamera().position;

            RaycastHit hit;
            if (Physics.Raycast(position, Vector3.down, out hit, LayerMask.NameToLayer("Ground")))
            {
                position = hit.point;
            }

            Vector3 forward = Vector3.ProjectOnPlane(VRTK_DeviceFinder.HeadsetCamera().forward, Vector3.up);

            VIVE_Teleporter.GoToMenu(position, Quaternion.LookRotation(forward));
        }

        if (OnGoToMenu != null)
        {
            OnGoToMenu(null, null);
        }
#endif
    }

    private void ToGameplay()
    {
        if (_cameraController != null)
        {
            _cameraController.CameraMode = CameraMode.Gameplay;
        }
        _menuNavigationController.PopMenuScreen();
        _menuNavigationController.HidePopup();
        _menuNavigationController.IsVisible = false;
        Time.timeScale = 1;
#if VIVE
        VIVE_Teleporter.GoToGameplay();
        VIVE_Teleporter.GoToCachedPosition();
        if (OnGoToGameplay != null)
        {
            OnGoToGameplay(null, null);
        }

#endif
}

public void ResetGame()
    {
        _singlePlayerController.ResetGame();
        _tutorialController.ResetTutorial();
    }

    public void OnGameOver()
    {
        ToMenu(true);
        _menuNavigationController.ShowPopup(_gameOverPopupFactory.Create().gameObject);
    }

    private void ToggleMenu(object sender, ControllerInteractionEventArgs e)
    {
        if (_menuNavigationController.IsVisible)
        {
            _menuNavigationController.OnAppButtonPressed();
        }
        else
        {
            ToMenu();
            _menuNavigationController.ShowPopup(_pausePopupFactory.Create().gameObject);
        }
    }

    public void Tick()
    {
#if GVR
        if (GvrController.AppButtonUp)
        {
            ToggleMenu();
        }
#endif

#if VIVE

#endif
    }


}
