﻿using UnityEngine;

public enum CameraMode
{
    Menu,
    Gameplay
}

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private GameObject _gvrControllerPointer;

    private Transform _cameraMenuPosition;
    private Transform _cameraGameplayPosition;

    private CameraMode _cameraMode = CameraMode.Menu;

    public void Init(Transform cameraMenuPosition, Transform cameraGameplayPosition)
    {
        _cameraMenuPosition = cameraMenuPosition;
        _cameraGameplayPosition = cameraGameplayPosition;

        transform.parent.position = _cameraMenuPosition.position;
    }

    public CameraMode CameraMode {
        get { return _cameraMode; }
        set
        {
            if (_cameraMode == value) return;
            _cameraMode = value;
            transform.parent.position = value == CameraMode.Menu
                ? _cameraMenuPosition.position
                : _cameraGameplayPosition.position;
            transform.parent.rotation = value == CameraMode.Menu
                ? _cameraMenuPosition.rotation
                : _cameraGameplayPosition.rotation;
            _gvrControllerPointer.SetActive(CameraMode == CameraMode.Menu);
        }
    }
}
