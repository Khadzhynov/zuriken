﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;

public class SteamworksTest : MonoBehaviour
{
    protected Callback<GameOverlayActivated_t> m_GameOverlayActivated;

    private CallResult<NumberOfCurrentPlayers_t> m_NumberOfCurrentPlayers;

    private CallResult<LeaderboardFindResult_t> m_LeaderboardFound;

    private SteamLeaderboard_t m_leaderboard;

    private SteamLeaderboardEntries_t m_LeaderboardContent;

    private void OnEnable()
    {
        Debug.Log("Subscribing Game Overlay");
        if (SteamManager.Initialized)
        {
            Debug.Log("Steam manager initialized");

            m_GameOverlayActivated = Callback<GameOverlayActivated_t>.Create(OnGameOverlayActivated);

            m_NumberOfCurrentPlayers = CallResult<NumberOfCurrentPlayers_t>.Create(OnNumberOfCurrentPlayers);

            m_LeaderboardFound = CallResult<LeaderboardFindResult_t>.Create(OnLeaderboardFound);

            //m_LeaderboardContent = CallResult<LeaderboardFindResult_t>.Create(OnLeaderboardFound);
        }
        GetUserName();
    }

    private void OnLeaderboardFound(LeaderboardFindResult_t pCallback, bool bIOFailure)
    {
        m_leaderboard = pCallback.m_hSteamLeaderboard;
        Debug.Log("Leaderboard Found: " + pCallback.m_hSteamLeaderboard.m_SteamLeaderboard.ToString());

    }

    private void OnNumberOfCurrentPlayers(NumberOfCurrentPlayers_t pCallback, bool bIOFailure)
    {
        if (pCallback.m_bSuccess != 1 || bIOFailure)
        {
            Debug.Log("There was an error retrieving the NumberOfCurrentPlayers.");
        }
        else
        {
            Debug.Log("The number of players playing your game: " + pCallback.m_cPlayers);
        }
    }

    private void OnGameOverlayActivated(GameOverlayActivated_t pCallback)
    {
        if (pCallback.m_bActive != 0)
        {
            Debug.Log("Steam Overlay has been activated");
        }
        else
        {
            Debug.Log("Steam Overlay has been closed");
        }
    }

    void Start()
    {
        
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            Debug.Log("Do the steam magic!");
            GetUserName();
            GetNumberOfPlayers();
            GetFriends();
        }

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Do the steam magic via mouse!");
            GetUserName();
            GetNumberOfPlayers();
            GetFriends();
        }
    }

    /*
    private void FindOrCreateLeaderboard()
    {
        SteamAPICall_t handle = SteamUserStats.FindOrCreateLeaderboard("MyLeaderboard", ELeaderboardSortMethod.k_ELeaderboardSortMethodDescending, ELeaderboardDisplayType.k_ELeaderboardDisplayTypeNumeric);

        m_LeaderboardFound.Set(handle);
    }

    private void DownloadLeaderboardContent()
    {
        SteamAPICall_t handle = SteamUserStats.DownloadLeaderboardEntries(m_leaderboard, ELeaderboardDataRequest.k_ELeaderboardDataRequestGlobal, 0, 100);

    }
    */

    private void GetNumberOfPlayers()
    {
        if (SteamManager.Initialized)
        {
            SteamAPICall_t handle = SteamUserStats.GetNumberOfCurrentPlayers();

            m_NumberOfCurrentPlayers.Set(handle);
        }
    }

    private void GetFriends()
    {
        int friendCount = SteamFriends.GetFriendCount(EFriendFlags.k_EFriendFlagImmediate);
        Debug.Log("[STEAM-FRIENDS] Listing " + friendCount + " Friends.");
        for (int i = 0; i < friendCount; ++i)
        {
            CSteamID friendSteamId = SteamFriends.GetFriendByIndex(i, EFriendFlags.k_EFriendFlagImmediate);
            string friendName = SteamFriends.GetFriendPersonaName(friendSteamId);
            EPersonaState friendState = SteamFriends.GetFriendPersonaState(friendSteamId);

            Debug.Log(friendName + " is " + friendState);
        }
    }

    private static void GetUserName()
    {
        if (SteamManager.Initialized)
        {
            string name = SteamFriends.GetPersonaName();
            Debug.Log("Player name: " + name);
        }
    }
}
