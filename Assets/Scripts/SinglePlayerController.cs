﻿public interface IGameOverHandler
{
    void OnGameOver();
}

public class SinglePlayerController : IPlayerDeathHandler
{
    private readonly EnemySpawner _spawnController;
    private readonly Score _score;
    private readonly HighScores _highScores;
    private readonly PlayerCollision _playerCollision;
    private IGameOverHandler _gameOverHandler;

    public static event System.EventHandler OnGameReset;

    public SinglePlayerController(EnemySpawner spawnController, Score score, PlayerCollision playerCollision, HighScores highScores)
    {
        _spawnController = spawnController;
        _score = score;
        _highScores = highScores;
        _playerCollision = playerCollision;
    }

    public void SetGameOverHandler(IGameOverHandler handler)
    {
        _gameOverHandler = handler;
    }

    public void AddScore(int value)
    {
        _score.AddScore(value);
    }

    public void StartGame()
    {
        ResetGame();
        _spawnController.StartSpawn();
        _playerCollision.Reset();
    }

    public void OnPlayerDeath()
    {
        _highScores.AddScore(_score.Value);
        if (_gameOverHandler != null)
            _gameOverHandler.OnGameOver();
    }

    public void ResetGame()
    {
        _spawnController.StopSpawn();
        _score.SetToZero();
        if (OnGameReset != null)
        {
            OnGameReset(null, null);
        }
    }
}