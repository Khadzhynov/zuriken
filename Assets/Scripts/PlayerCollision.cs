﻿#define VIVE
//#define GVR

using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public interface IPlayerDeathHandler
{
    void OnPlayerDeath();
}

public class PlayerCollision : MonoBehaviour
{

    [SerializeField] private Camera _camera;

    [SerializeField] private Image _splat;

    private Animator _animator;
    private bool _isDead;

    private IPlayerDeathHandler _playerDeathHandler;
    private Transform _from;
    private Transform _zombieHeadTransform;

    public void Reset() {
        _isDead = false;
        _canSlerp = false;
        _fraction = 0f;
    }

    void Start() {
        _animator = GetComponent<Animator>();

#if VIVE
        _camera = Camera.main;
#endif

    }
    public void SetPlayerDeathHandler(IPlayerDeathHandler handler)
    {
        _playerDeathHandler = handler;
    }


    private void OnTriggerEnter(Collider other)
    {
        ProcessCollision(other.gameObject);
    }

    void OnCollisionEnter(Collision other)
    {
        ProcessCollision(other.gameObject);
    }

    private void ProcessCollision(GameObject other)
    {
        if (_isDead)
            return;

        if (other.gameObject.tag == "Zombie")
        {
            _isDead = true;

            FindZombieHead(other.gameObject.transform);
#if GVR
            _animator.Play("Death");
#endif
            StartCoroutine(Death(4.17f)); //duration of biting animation
        }
    }

    private void FindZombieHead(Transform parent)
    {
        foreach (Transform child in parent) {
            if (child.gameObject.tag == "Head") {
                _zombieHeadTransform = child.gameObject.transform;
            }
            else {
                FindZombieHead(child);
            }
        }
    }

    private float _duration = 2f;
    private float _fraction;
    private float _comparisionDelta = 0.05f;
    private bool _canSlerp;

    void Update()
    {
        if (_isDead && _canSlerp) {
            if (_fraction + _comparisionDelta < 1) {
                _fraction += Time.deltaTime/_duration;
                if (_from != null && _zombieHeadTransform != null) {
                    _camera.transform.rotation = Quaternion.Slerp(_from.rotation,
                        Quaternion.LookRotation(_zombieHeadTransform.position - _from.position), _fraction);
                }
            }
            else {
                _canSlerp = false;
            }
        }
    }

    private IEnumerator Death(float delay)
    {
        if (_camera == null)
        {
            _camera = Camera.main;
        }

        if (_splat != null)
        {
            _splat.enabled = true;
        }

        _from = _camera.transform;
        _canSlerp = true;
        yield return new WaitForSeconds(delay);
        if (_playerDeathHandler != null)
            _playerDeathHandler.OnPlayerDeath();

        if (_splat != null)
        {
            _splat.enabled = false;
        }
    }
}

