﻿using System;
using UnityEngine;

public class Settings
{
    public const string SkipTutorialKey = "SkipTutorial";
    public const string SoundVolumeKey = "SoundVolume";

    public Settings()
    {
        SkipTutorial = PlayerPrefsX.GetBool(SkipTutorialKey);
        SoundVolume = PlayerPrefs.GetFloat(SoundVolumeKey, 1f);
    }

    public event Action<bool> OnSkipTutorialChanged;
    public bool SkipTutorial { get; private set; }
    public void SetSkipTutorial(bool skipTutorial)
    {
        if (SkipTutorial == skipTutorial) return;
        SkipTutorial = skipTutorial;
        if (OnSkipTutorialChanged != null) OnSkipTutorialChanged(SkipTutorial);
    }

    public event Action<float> OnSoundVolumeChanged; 
    public float SoundVolume { get; private set; }
    public void SetSoundVolume(float soundVolume)
    {
        if (Math.Abs(soundVolume - SoundVolume) < Mathf.Epsilon) return;
        SoundVolume = soundVolume;
        if (OnSoundVolumeChanged != null) OnSoundVolumeChanged(SoundVolume);
    }

    public void Save()
    {
        PlayerPrefsX.SetBool(SkipTutorialKey, SkipTutorial);
        PlayerPrefs.SetFloat(SoundVolumeKey, SoundVolume);
        PlayerPrefs.Save();
    }
}
