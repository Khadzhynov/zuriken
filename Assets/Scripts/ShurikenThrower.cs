﻿//#define GVR
#define VIVE

using System;
using System.Collections;
using Smoother;
using UnityEngine;
using VRTK;

public class ShurikenThrower : MonoBehaviour
{
    public event Action OnShurikenCreated;
    public event Action OnShurikenThrown; 

    private GvrAudioSource _audioSource;
    private Config _config;
    private ShurikenFactory _shurikenFactory;
    private Transform _gaze;
    private Hand _hand;

    private bool _isControllerConnected;
    private GameObject _shuriken;

    private Vector3Smoother _velocitySmoother;
    private Vector3? _lastShurikenPosition;

    private void Awake()
    {
        _audioSource = GetComponent<GvrAudioSource>();
    }

    public void Init(Config config, ShurikenFactory shurikenFactory, Transform cameraTransform)
    {
#if GVR
        _config = config;
        _shurikenFactory = shurikenFactory;
        _gaze = cameraTransform;
        _hand = Instantiate(GvrSettings.Handedness == GvrSettings.UserPrefsHandedness.Left
            ? _config.LeftHandPrefab
            : _config.RightHandPrefab);
        _hand.transform.SetParent(transform, false);
        _hand.Animator.Play("Idle");
#endif

#if VIVE

        _config = config;
        _shurikenFactory = shurikenFactory;
        _gaze = cameraTransform;

        Debug.Log("Instantiate hand");
        _hand = Instantiate(_config.RightHandPrefab);

        /*
        _hand = Instantiate(GvrSettings.Handedness == GvrSettings.UserPrefsHandedness.Left
            ? _config.LeftHandPrefab
            : _config.RightHandPrefab);
            */

        _hand.transform.SetParent(transform, false);
        //_hand.Animator.Play("Idle");

        Debug.Log("Subscribe SHOOT to OnTrigger");
        VRTK_Listener.OnGripPressed += SpawnShuriken;
        VRTK_Listener.OnGripReleased += OnShurikenReleased;

#endif
    }

    private void OnDestroy()
    {
        VRTK_Listener.OnGripPressed -= SpawnShuriken;
        VRTK_Listener.OnGripReleased -= OnShurikenReleased;
    }

    public void OnGameEnd()
    {
        _canThrow = true;
    }

    private void Update()
    {
        if (Time.timeScale == 0) return;

#if GVR
        _isControllerConnected = GvrController.State == GvrConnectionState.Connected;
        _hand.gameObject.SetActive(_isControllerConnected);
        if (!_isControllerConnected)
            return;

        ButtonController();
        UpdateHand();
#endif

    }

    private void ButtonController()
    {
#if UNITY_EDITOR && GVR
        if (GvrController.TouchDown)
        {
            if (GvrController.TouchUp)
                return;
#endif

#if GVR
        if (GvrController.ClickButtonDown) 
        {
            if (GvrController.ClickButtonUp)
                return;
#endif

#if VIVE
        if (false) 
        {

#endif

        CreateShuriken();
            return;
        }

#if UNITY_EDITOR && GVR
        if (_shuriken != null && GvrController.TouchUp)
        {
#endif

#if GVR
        if (_shuriken != null && GvrController.ClickButtonUp) 
        {
#endif

#if VIVE

        if (false)
        {

#endif
            Throw();
            return;
        }
    }


#if VIVE
    private void SpawnShuriken(object sender, ControllerInteractionEventArgs e)
    {
        //CreateShuriken();
        //Throw();
    }

    private void OnShurikenReleased(object sender, ControllerInteractionEventArgs e)
    {
        //_hand.Animator.Play("ThrowFrisbee");
        StartCoroutine(ChangeCanThrowState());
        _shuriken.transform.parent = null;
        _audioSource.Play();

        var trailRenderer = _shuriken.GetComponent<TrailRenderer>();
        trailRenderer.enabled = true;
        Destroy(_shuriken, 3f);

        _shuriken = null;

        if (OnShurikenThrown != null)
            OnShurikenThrown();
    }

#endif

    private void FixedUpdate()
    {
        if (_shuriken != null)
            UpdateShurikenPhysics();
    }

    private void UpdateShurikenPhysics()
    {
        if (_lastShurikenPosition != null)
        {
            var velocity = (_shuriken.transform.position - _lastShurikenPosition.Value) / Time.fixedDeltaTime;
            if (Vector3.Dot(_gaze.forward, velocity) > 0)
                _velocitySmoother.AddValue(velocity);
        }
        _lastShurikenPosition = _shuriken.transform.position;
    }

    private void UpdateHand()
    {
#if GVR
        _hand.transform.localRotation = Quaternion.Lerp(_hand.transform.localRotation,
            GvrController.ArmModel.wristRotation, Time.deltaTime * 25f);
        _hand.transform.localPosition = Vector3.Lerp(_hand.transform.localPosition, GvrController.ArmModel.wristPosition,
            Time.deltaTime * 25f);
#endif

    }

    private void CreateShuriken()
    {
        _velocitySmoother = new Vector3Smoother(30);

        //_hand.Animator.Play("TakeFrisbee");

        //foreach (Transform go in _hand.PlaceForShuriken) Destroy(go.gameObject);

        _shuriken = _shurikenFactory.Create();

        var controller = VRTK_ControllerReference.GetControllerReference(SDK_BaseController.ControllerHand.Right);

        var arg = new InteractableObjectEventArgs();
        arg.interactingObject = controller.actual;


        Transform placeHolder = Hand.GetSurikenPlace();
        _shuriken.transform.SetParent(placeHolder);

        _shuriken.transform.localPosition = Vector3.zero;
        _shuriken.transform.localScale = Vector3.one * 0.3f;


        Debug.Log("Set Shuriken grabbed");
        _shuriken.GetComponent<VRTK_InteractableObject>().OnInteractableObjectGrabbed(arg);

        

        //_shuriken.transform.localPosition = Vector3.zero;

        //_shuriken.transform.localPosition = controller.actual.transform.position;

        _shuriken.transform.localRotation = Quaternion.identity;
        //_lastShurikenPosition = _shuriken.transform.position;
        if (OnShurikenCreated != null) OnShurikenCreated();
    }



    private bool _canThrow = true;
    private void Throw()
    {
        if (_shuriken == null || !_canThrow)
            return;

        var velocityMagnitude = _velocitySmoother.Value.magnitude;
        Vector3 velocity = (_velocitySmoother.Value.normalized + 2 * _gaze.forward).normalized * velocityMagnitude;

        _hand.Animator.Play("ThrowFrisbee");
        StartCoroutine(ChangeCanThrowState());
        _shuriken.transform.parent = null;
        _audioSource.Play();

        //Debug.DrawLine(transform.position, transform.position + _throwVelocity, Color.cyan, 5);

        _shuriken.AddComponent<FrisbeeController>().Launch(velocity * 20, Mathf.PI * velocity.magnitude * 10);

        var trailRenderer = _shuriken.GetComponent<TrailRenderer>();
        trailRenderer.enabled = true;
        Destroy(_shuriken, 3f);
        _shuriken = null;
        if (OnShurikenThrown != null)
            OnShurikenThrown();
    }

    private IEnumerator ChangeCanThrowState()
    {
        _canThrow = false;
        yield return new WaitForSeconds(0.75f);
        _canThrow = true;
    }

    [Serializable]
    public class Config
    {
        public GameObject ShurikenPrefab;
        public Hand RightHandPrefab;
        public Hand LeftHandPrefab;

        public GameObject VIVE_ShurikenPrefab;
        public Hand VIVE_RightHandPrefab;
        public Hand VIVE_LeftHandPrefab;

    }
}