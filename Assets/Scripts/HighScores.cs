﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HighScores {

    public const string HighScoresKey = "Highscores";
    private readonly List<int> _highScores;

    public HighScores() {
        _highScores = new List<int>(PlayerPrefsX.GetIntArray(HighScoresKey));
    } 

    public List<int> GetHighScores() {
        return _highScores;
    }

    public int GetHighestScore() {
        return _highScores[0];
    }

    public void AddScore(int score) {
        if (!_highScores.Contains(score)) {
            _highScores.Add(score);
            SaveHighScores();
        }
    }

    public void SaveHighScores() {
        _highScores.Sort((a, b) => -1 * a.CompareTo(b));
        if (_highScores.Count > 10) {
            _highScores.RemoveRange(10, _highScores.Count - 10);
        }
        PlayerPrefsX.SetIntArray(HighScoresKey, _highScores.ToArray());
    }
}
