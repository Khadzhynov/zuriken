﻿using UnityEngine;
using UnityEngine.EventSystems;

public class SwitchOption : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private GameObject _selectionGameObject;

    private Switch _switch;
    public void Init(Switch s)
    {
        _switch = s;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        _switch.SelectedOption = this;
    }

    public void SetSelected(bool isSelected)
    {
        _selectionGameObject.SetActive(isSelected);
    }
}
