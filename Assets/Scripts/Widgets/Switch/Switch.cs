﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class Switch : MonoBehaviour
{
    [Serializable]
    public class SwitchOptionEvent : UnityEvent<SwitchOption> { }
    public SwitchOptionEvent OptionSelectedEvent;

    private SwitchOption[] _options;

    private SwitchOption _selectedOption;
    public SwitchOption SelectedOption
    {
        get
        {
            return _selectedOption;
        }
        set
        {
            if (value == _selectedOption) return;
            _selectedOption = value;
            foreach (var switchOption in _options)
            {
                switchOption.SetSelected(switchOption == value);
            }
            OptionSelectedEvent.Invoke(value);
        }
    }

    void Awake()
    {
        _options = GetComponentsInChildren<SwitchOption>();
        foreach (var switchOption in _options)
        {
            switchOption.Init(this);
        }
        if (_options.Length > 0)
            SelectedOption = _options[0];
    }
}