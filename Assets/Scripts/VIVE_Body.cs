﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class VIVE_Body : MonoBehaviour
{

    private static VIVE_Body _body;

    [SerializeField]
    private VRTK_BodyPhysics _bodyPhysics;

    private void Awake()
    {
        _body = this;
    }

    
    public static GameObject GetBody()
    {
        GameObject result = _body._bodyPhysics.GetBodyColliderContainer();

        if (result == null)
        {
            Debug.LogError("Can not find body game objectS");
        }

        return result;
    }
    
}
