﻿
using System;
using UnityEngine;

[CreateAssetMenu(fileName = "Configs", menuName = "Zuriken/Configs")]
public class Configs : ScriptableObject
{
    public ShurikenThrower.Config ShurikenThrower;
    public EnemySpawner.Config EnemySpawner;
    public TutorialController.Config Tutorial;
    public MenuConfig Menu;

    [Serializable]
    public class MenuConfig
    {
        public MainMenu MainMenuPrefab;
        public SettingsMenu SettingsMenuPrefab;
        public TutorialPopup TutorialPopupPrefab;
        public PausePopup PausePopupPrefab;
        public GameOverPopup GameOverPopupPrefab;
        public HighScoresPopup HighScoresPopupPrefab;
        public CreditsPopup CreditsPopupPrefab;
    }
}
