﻿using UnityEngine;

public class FrisbeeController : MonoBehaviour
{
    private FrisbeeModel _model;
    private Rigidbody _rigidbody;

    private bool _isSimulate = true;

    public void Launch(Vector3 vector, float spin)
    {
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.isKinematic = false;
        _model = new FrisbeeModel(transform.position, transform.rotation, vector, spin, Vector3.zero);
    }

    void FixedUpdate()
    {
        if (_model == null) return;
        if (!_isSimulate) return;

        _model.Simulate(Time.fixedDeltaTime);

        Debug.DrawLine(transform.position, _model.Position, Color.blue, 10f);

        _rigidbody.position = _model.Position;
        _rigidbody.rotation = _model.Rotation;
    }

    private void OnCollisionEnter(Collision collision)
    {
        UnityPhysics();
    }

    private void UnityPhysics()
    {
        _isSimulate = false;

        if (_rigidbody.useGravity)
            return;

        _rigidbody.useGravity = true;
        _rigidbody.isKinematic = false;
        _rigidbody.velocity = transform.forward;

        Destroy(gameObject, 10.0f);
    }
}