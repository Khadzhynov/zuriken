﻿using UnityEngine;

public class FrisbeeModel {

    /// <summary>
    /// The acceleration of gravity (m/s^2).
    /// </summary>
    private const float Gravity = -9.81f;
    
    /// <summary>
    /// The mass of a standard frisbee in kilograms.
    /// </summary>
    private const float Mass = 0.15f;
    
    /// <summary>
    /// The density of air in kg/m^3.
    /// </summary>
    private const float Rho = 1.23f;
    
    /// <summary>
    /// The area of a standard frisbee.
    /// </summary>
    private const float Area = 0.01f;

    private const float DragCoefficient = 2.72f; //2.72f;
    private const float TiltStabilizationFactor = 0.01f;//0.001f;
    private const float SpinFriction = 0.001f;//0.03f;

    private readonly Vector3 _wind;

    private readonly float _dragRatio;

    public FrisbeeModel(Vector3 startPosition, Quaternion startRotation, Vector3 launchVector, float spin, Vector3 wind) {

        _velocity = launchVector;
        _spin = spin;
        _wind = wind;

        Position = startPosition;
        Rotation = startRotation;

        _dragRatio = Rho * Area * DragCoefficient / 2 / Mass;
    }

    private Vector3 _velocity;
    private float _spin;

    public Vector3 Position { get; private set; }
    public Quaternion Rotation { get; private set; }

    public void Simulate(float deltaTime) {
        Vector3 normal = Rotation * Vector3.up;
        Vector3 drag = (-_velocity + _wind) * _dragRatio;
        float alpha = Vector3.Angle(normal, drag);
        Vector3 gravity = Mass * Gravity * Vector3.up;
        Vector3 lift = normal * drag.magnitude * Mathf.Cos(Mathf.Deg2Rad * alpha);
        Vector3 force = gravity + lift;
        Vector3 acceleration = force / Mass;

        Debug.DrawLine(Position, Position + normal, Color.green);
        Debug.DrawLine(Position, Position + drag, Color.black);
        Debug.DrawLine(Position, Position + force, Color.white);

        _velocity += acceleration * deltaTime;
        _spin -= SpinFriction * deltaTime;

        Position += _velocity * deltaTime;
        Rotation = Quaternion.Lerp(Rotation, Quaternion.identity, TiltStabilizationFactor);
        Rotation *= Quaternion.Euler(0, _spin * Mathf.Rad2Deg * deltaTime, 0);
    }
}