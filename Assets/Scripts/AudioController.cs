﻿
using UnityEngine;

[RequireComponent(typeof(GvrAudioListener))]
public class AudioController : MonoBehaviour
{
    [SerializeField]
    private float _minGainDb = -50f;

    [SerializeField]
    private float _maxGainDb = 0f;

    private GvrAudioListener _audioListener;


    // Use this for initialization
    void Awake()
    {
        _audioListener = GetComponent<GvrAudioListener>();
    }

    public void Init(Settings settings)
    {
        SetVolume(settings.SoundVolume);
        settings.OnSoundVolumeChanged += SetVolume;
    }

    private void SetVolume(float volume)
    {
        _audioListener.globalGainDb = Mathf.Lerp(_minGainDb, _maxGainDb, volume);
    }
}