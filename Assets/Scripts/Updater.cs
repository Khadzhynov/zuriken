﻿using System.Collections.Generic;
using UnityEngine;

public interface ITickable
{
    void Tick();
}

public class Updater : MonoBehaviour
{
    private List<ITickable> _tickables = new List<ITickable>();

    public void AddListener(ITickable tickable)
    {
        _tickables.Add(tickable);
    }
	
	// Update is called once per frame
	void Update () {
	    for (var i = 0; i < _tickables.Count; ++i)
	    {
	        _tickables[i].Tick();
	    }
    }
}