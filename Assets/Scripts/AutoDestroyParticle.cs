﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroyParticle : MonoBehaviour {
    [SerializeField] private bool _autoDestroy;
    private ParticleSystem _particleSystem;
	// Use this for initialization

	void Start () {
	    _particleSystem = GetComponent<ParticleSystem>();
	    if (_autoDestroy) {
	        StartCoroutine(DestroyAfterDelay(_particleSystem.main.duration));
	    }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private IEnumerator DestroyAfterDelay(float delay) {
        yield return new WaitForSeconds(delay);
        Destroy(gameObject);
    }
}
