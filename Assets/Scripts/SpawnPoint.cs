﻿using UnityEngine;
using UnityEngine.AI;

public class SpawnPoint : MonoBehaviour
{
    [SerializeField]
    private GameObject[] _zombiePrefabs;

    public bool IsEnemyLayingState;

    public GameObject Spawn()
    {
        var zombie = Instantiate(_zombiePrefabs[Random.Range(0, _zombiePrefabs.Length)]);
        zombie.transform.position = transform.position;
        zombie.transform.rotation = transform.rotation;
        var navMeshAgent = zombie.GetComponent<NavMeshAgent>();
        if (navMeshAgent)
            navMeshAgent.Warp(transform.position);
        return zombie;
    }
}
