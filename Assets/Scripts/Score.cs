﻿using System;

public class Score
{
    private int _value;

    public int Value 
    {
        get { return _value; }
    }

    public event Action<int> OnValueChanged;

    public void AddScore(int score)
    {
        _value += score;
        if (OnValueChanged != null) OnValueChanged(_value);
    }

    public void SetToZero()
    {
        _value = 0;
        if (OnValueChanged != null) OnValueChanged(_value);
    }
}
