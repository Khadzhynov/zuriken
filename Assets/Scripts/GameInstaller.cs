﻿//#define GVR
#define VIVE
using UnityEngine;
using VRTK;

public class GameInstaller : MonoBehaviour
{
    [SerializeField]
    private ScoreView _scoreView;

    [SerializeField]
    private Transform _cameraMenuTransform;

    [SerializeField]
    private Transform _cameraGameplayTransform;

    [SerializeField]
    private CameraController _cameraController;

    [SerializeField]
    private AudioController _audioController;

    [SerializeField]
    private PlayerCollision _playerCollision;

    [SerializeField]
    private Transform _enemiesRootTransform;

    [SerializeField]
    private Configs _configs;

    [SerializeField]
    private ShurikenThrower _shurikenThrower;

    [SerializeField]
    private EnemySpawner _enemySpawner;

    [SerializeField]
    private TutorialController _tutorialController;

    [SerializeField]
    private Transform _menuRootTransform;

    private Settings _settings;
    private HighScores _highScores;


    [Header("VIVE settings")]

    [SerializeField]
    private Transform _ViveRightHandPivot;

    [SerializeField]
    private Transform _ViveLeftHandPivot;


    // Use this for initialization
    void Start () {
		InstallBindings();
	}

    private void InstallBindings()
    {
        var score = new Score();
        _scoreView.Init(score);

#if GVR
        var shurikenFactory = new ShurikenFactory(_configs.ShurikenThrower.ShurikenPrefab, score);
#endif

#if VIVE
        var shurikenFactory = new ShurikenFactory(_configs.ShurikenThrower.VIVE_ShurikenPrefab, score);
#endif

        _settings = new Settings();
        _highScores = new HighScores();

        if (_audioController != null)
        {
            _audioController.Init(_settings);
        }

        if (_cameraController != null)
        {
            _cameraController.Init(_cameraMenuTransform, _cameraGameplayTransform);
        }

        if (_enemySpawner != null)
        {
            _enemySpawner.Init(_configs.EnemySpawner, _playerCollision.transform, _enemiesRootTransform);
        }

        var tutorialStepFactory = new TutorialStepFactory(_shurikenThrower, _playerCollision.transform, _enemiesRootTransform);
        _tutorialController.Init(_configs.Tutorial, tutorialStepFactory, _enemiesRootTransform);

        var singlePlayerController = new SinglePlayerController(_enemySpawner, score, _playerCollision, _highScores);
        _playerCollision.SetPlayerDeathHandler(singlePlayerController);

        var menuNavigationController = new MenuNavigationController(_menuRootTransform);

        var creditsPopupFactory = new CreditsPopupFactory(_configs.Menu.CreditsPopupPrefab, menuNavigationController);
        var settingsMenuFactory = new SettingsMenuFactory(_configs.Menu.SettingsMenuPrefab, _settings, menuNavigationController, creditsPopupFactory);

        var mainMenu = Instantiate(_configs.Menu.MainMenuPrefab);
        var highScoresPopupFactory = new HighScoresPopupFactory(_configs.Menu.HighScoresPopupPrefab, menuNavigationController, _highScores);

        mainMenu.Init(menuNavigationController, settingsMenuFactory, highScoresPopupFactory);
        menuNavigationController.PushMenuScreen(mainMenu.gameObject);

        var tutorialPopupFactory = new TutorialPopupFactory(_configs.Menu.TutorialPopupPrefab, menuNavigationController);
        var pausePopupFactory = new PausePopupFactory(_configs.Menu.PausePopupPrefab, menuNavigationController);
        var gameOverPopupFactory = new GameOverPopupFactory(_configs.Menu.GameOverPopupPrefab, menuNavigationController, score, _highScores);


        var gameController = new GameController(menuNavigationController, _cameraController, tutorialPopupFactory,
            pausePopupFactory, gameOverPopupFactory, singlePlayerController, _tutorialController);


        singlePlayerController.SetGameOverHandler(gameController);
        _tutorialController.SetTutorialCompletedHandler(gameController.StartGame);

        mainMenu.SetStartGameHandler(gameController);
        settingsMenuFactory.SetStartTutorialHandler(gameController);
        tutorialPopupFactory.SetStartTutorialHandler(gameController);

        pausePopupFactory.SetResumeGameHandler(gameController);
        pausePopupFactory.SetResetGameHandler(gameController);

        gameOverPopupFactory.SetRestartGameHandler(gameController);
        gameOverPopupFactory.SetResetGameHandler(gameController);


        var updater = new GameObject("Updater").AddComponent<Updater>();
        updater.AddListener(gameController);

        if (gameController != null)
        {
            gameController.Start();
        }


#if VIVE
        
        VIVE_Teleporter.GoToMenu(VIVE_Teleporter.Instance.GamePosition.position, VIVE_Teleporter.Instance.GamePosition.rotation);
#endif
    }

    void OnDestroy()
    {
        _settings.Save();
    }
}