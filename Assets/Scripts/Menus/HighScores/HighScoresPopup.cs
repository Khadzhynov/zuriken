﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class HighScoresPopup : MonoBehaviour {

    [SerializeField] private Button _backToMainMenuButton;
    [SerializeField] private Text _positionsText;
    [SerializeField] private Text _scoresText;

    private MenuNavigationController _menuNavigationController;

    private void Awake() {
        _backToMainMenuButton.onClick.AddListener(BackToMainMenuPressed);
    }

    public void Init(MenuNavigationController menuNavigationController, HighScores highScores) {
        _menuNavigationController = menuNavigationController;
        DisplayHighScores(highScores.GetHighScores());
    }

    private void DisplayHighScores(List<int> highscores) {
        for (int i = 0; i < highscores.Count; i++)
        {
            int index = i + 1;
            _positionsText.text += index + ". " + "\n";
            _scoresText.text += highscores[i] + "\n";
        }
    }

    private void BackToMainMenuPressed() {
        _menuNavigationController.PopMenuScreen();
    }

    private void OnDestroy() {
        _backToMainMenuButton.onClick.RemoveAllListeners();
    }
}
