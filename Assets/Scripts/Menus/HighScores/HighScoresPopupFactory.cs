﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighScoresPopupFactory {

    private readonly HighScoresPopup _highScoresPopupPrefab;
    private readonly MenuNavigationController _menuNavigationController;
    private readonly HighScores _highScores;

    public HighScoresPopupFactory(HighScoresPopup highScoresPopupPrefab, MenuNavigationController menuNavigationController, HighScores highScores) {
        _highScoresPopupPrefab = highScoresPopupPrefab;
        _menuNavigationController = menuNavigationController;
        _highScores = highScores;
    }

    public HighScoresPopup Create() {
        var highscoresPopup = Object.Instantiate(_highScoresPopupPrefab);
        highscoresPopup.Init(_menuNavigationController, _highScores);
        return highscoresPopup;
    }
}
