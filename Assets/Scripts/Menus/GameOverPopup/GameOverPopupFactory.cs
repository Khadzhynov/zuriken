﻿using UnityEngine;

public class GameOverPopupFactory
{
    private readonly GameOverPopup _gameOverPopup;
    private readonly MenuNavigationController _menuNavigationController;
    private readonly Score _score;
    private readonly HighScores _highScores;
    private IRestartGameHandler _restartGameHandler;
    private IResetGameHandler _resetGameHandler;

    public GameOverPopupFactory(GameOverPopup gameOverPopup, MenuNavigationController menuNavigationController, Score score, HighScores highScores)
    {
        _gameOverPopup = gameOverPopup;
        _menuNavigationController = menuNavigationController;
        _score = score;
        _highScores = highScores;
    }

    public void SetRestartGameHandler(IRestartGameHandler handler)
    {
        _restartGameHandler = handler;
    }

    public void SetResetGameHandler(IResetGameHandler handler)
    {
        _resetGameHandler = handler;
    }

    public GameOverPopup Create()
    {
        var gameOverPopup = Object.Instantiate(_gameOverPopup);
        gameOverPopup.Init(_menuNavigationController, _score.Value, _highScores.GetHighestScore());
        if (_restartGameHandler != null)
            gameOverPopup.SetRestartGameHandler(_restartGameHandler);
        if (_resetGameHandler != null)
            gameOverPopup.SetResetGameHandler(_resetGameHandler);
        return gameOverPopup;
    }
}
