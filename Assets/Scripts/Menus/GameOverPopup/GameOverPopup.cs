﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public interface IRestartGameHandler
{
    void RestartGame();
}

public class GameOverPopup : MonoBehaviour
{
    [SerializeField]
    private Text _scoreText;

    [SerializeField]
    private Button _restartButton,
        _mainMenuButton;

    [SerializeField] private Text _highestScoreText;

    private Action _onRestartPressed, _onMainMenuPressed;

    private void Awake()
    {
        _restartButton.onClick.AddListener(OnResartPressed);
        _mainMenuButton.onClick.AddListener(OnMainMenuPressed);
    }

    private MenuNavigationController _menuNavigationController;
    private IRestartGameHandler _restartGameHandler;
    private IResetGameHandler _resetGameHandler;

    public void Init(MenuNavigationController menuNavigationController, int score, int highestScore)
    {
        _menuNavigationController = menuNavigationController;
        _scoreText.text = score.ToString();
        _highestScoreText.text = highestScore.ToString();
    }

    public void SetRestartGameHandler(IRestartGameHandler handler)
    {
        _restartGameHandler = handler;
    }

    public void SetResetGameHandler(IResetGameHandler resetGameHandler)
    {
        _resetGameHandler = resetGameHandler;
    }

    private void OnResartPressed()
    {
        _menuNavigationController.HidePopup();
        if (_restartGameHandler != null)
            _restartGameHandler.RestartGame();
    }

    private void OnMainMenuPressed()
    {
        _menuNavigationController.HidePopup();
        if (_resetGameHandler != null)
            _resetGameHandler.ResetGame();
    }
}