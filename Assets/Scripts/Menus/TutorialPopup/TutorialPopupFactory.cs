﻿using UnityEngine;

public class TutorialPopupFactory
{
    private readonly TutorialPopup _tutorialPopupPrefab;
    private readonly MenuNavigationController _menuNavigationController;
    private IStartTutorialHandler _handler;

    public TutorialPopupFactory(TutorialPopup tutorialPopupPrefab, MenuNavigationController menuNavigationController)
    {
        _tutorialPopupPrefab = tutorialPopupPrefab;
        _menuNavigationController = menuNavigationController;
    }

    public void SetStartTutorialHandler(IStartTutorialHandler startTutorialHandler)
    {
        _handler = startTutorialHandler;
    }

    public TutorialPopup Create()
    {
        var tutorialPopup = Object.Instantiate(_tutorialPopupPrefab);
        tutorialPopup.Init(_menuNavigationController);
        if (_handler != null)
            tutorialPopup.SetStartTutorialHandler(_handler);
        return tutorialPopup;
    }
}
