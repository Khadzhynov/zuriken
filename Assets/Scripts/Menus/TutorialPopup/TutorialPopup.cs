﻿using UnityEngine;
using UnityEngine.UI;

public class TutorialPopup : MonoBehaviour
{
    [SerializeField]
    private Button _startTutorialButton, _skipTutorialButton;

    private void Awake()
    {
        _startTutorialButton.onClick.AddListener(OnStartTutorialPressed);
        _skipTutorialButton.onClick.AddListener(OnSkipTutorialPressed);
    }

    private MenuNavigationController _menuNavigationController;
    private IStartTutorialHandler _startTutorialHandler;

    public void Init(MenuNavigationController menuNavigationController)
    {
        _menuNavigationController = menuNavigationController;
    }

    public void SetStartTutorialHandler(IStartTutorialHandler startTutorialHandler)
    {
        _startTutorialHandler = startTutorialHandler;
    }

    private void OnStartTutorialPressed()
    {
        if (_startTutorialHandler != null)
            _startTutorialHandler.StartTutorial();
    }

    private void OnSkipTutorialPressed()
    {
        _menuNavigationController.HidePopup();
    }
}
