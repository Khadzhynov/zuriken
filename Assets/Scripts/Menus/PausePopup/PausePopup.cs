﻿using System;
using UnityEngine;
using UnityEngine.UI;

public interface IResumeGameHandler
{
    void ResumeGame();
}

public class PausePopup : MonoBehaviour
{
    [SerializeField]
    private Button _resumeButton,
        _mainMenuButton;

    private Action _onResumePressed, _onMainMenuPressed;

    private void Awake()
    {
        _resumeButton.onClick.AddListener(ResumePressed);
        _mainMenuButton.onClick.AddListener(MainMenuPressed);
    }

    private MenuNavigationController _menuNavigationController;
    private IResumeGameHandler _resumeGameHandler;
    private IResetGameHandler _resetGameHandler;

    public void Init(MenuNavigationController menuNavigationController)
    {
        _menuNavigationController = menuNavigationController;
    }

    public void SetResumeGameHandler(IResumeGameHandler handler)
    {
        _resumeGameHandler = handler;
    }

    public void SetResetGameHandler(IResetGameHandler handler)
    {
        _resetGameHandler = handler;
    }

    private void ResumePressed()
    {
        _menuNavigationController.HidePopup();
        if (_resumeGameHandler != null)
            _resumeGameHandler.ResumeGame();
    }

    private void MainMenuPressed()
    {
        _menuNavigationController.HidePopup();
        if (_resetGameHandler != null)
            _resetGameHandler.ResetGame();
    }
}