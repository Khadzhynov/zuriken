﻿using UnityEngine;

public class PausePopupFactory
{
    private readonly PausePopup _pausePopupPrefab;
    private readonly MenuNavigationController _menuNavigationController;
    private IResumeGameHandler _resumeGameHandler;
    private IResetGameHandler _resetGameHandler;

    public PausePopupFactory(PausePopup pausePopupPrefab, MenuNavigationController menuNavigationController)
    {
        _pausePopupPrefab = pausePopupPrefab;
        _menuNavigationController = menuNavigationController;
    }

    public void SetResumeGameHandler(IResumeGameHandler handler)
    {
        _resumeGameHandler = handler;
    }

    public void SetResetGameHandler(IResetGameHandler handler)
    {
        _resetGameHandler = handler;
    }

    public PausePopup Create()
    {
        var pausePopup = Object.Instantiate(_pausePopupPrefab);
        pausePopup.Init(_menuNavigationController);
        if (_resumeGameHandler != null)
            pausePopup.SetResumeGameHandler(_resumeGameHandler);
        if (_resetGameHandler != null)
            pausePopup.SetResetGameHandler(_resetGameHandler);
        return pausePopup;
    }
}
