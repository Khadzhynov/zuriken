﻿using UnityEngine;
using UnityEngine.UI;

public interface IStartGameHandler
{
    void StartGame();
}

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private Button _startGameButton,
        _highScoresButton,
        _settingsButton;

    private void Awake()
    {
        _startGameButton.onClick.AddListener(OnStartGamePressed);
        _highScoresButton.onClick.AddListener(OnHighScoresPressed);
        _settingsButton.onClick.AddListener(OnSettingsPressed);
    }

    private MenuNavigationController _menuNavigationController;
    private SettingsMenuFactory _settingsMenuFactory;
    private HighScoresPopupFactory _highScoresPopupFactory;

    private IStartGameHandler _startGameHandler;

    public void Init(MenuNavigationController menuNavigationController, SettingsMenuFactory settingsMenuFactory, HighScoresPopupFactory highScoresPopupFactory)
    {
        _menuNavigationController = menuNavigationController;
        _settingsMenuFactory = settingsMenuFactory;
        _highScoresPopupFactory = highScoresPopupFactory;
    }

    public void SetStartGameHandler(IStartGameHandler handler)
    {
        _startGameHandler = handler;
    }

    private void OnStartGamePressed()
    {
        if (_startGameHandler == null)
        {
            Debug.LogError("Start game handler is NULL!");
            return;
        }
        _startGameHandler.StartGame();
    }

    private void OnHighScoresPressed() {
        var highScoresPopup = _highScoresPopupFactory.Create();
        _menuNavigationController.PushMenuScreen(highScoresPopup.gameObject);
    }

    private void OnSettingsPressed()
    {
        var settingsMenu = _settingsMenuFactory.Create();
        _menuNavigationController.PushMenuScreen(settingsMenu.gameObject);
    }
}
