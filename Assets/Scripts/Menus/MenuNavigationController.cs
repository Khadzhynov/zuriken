﻿using System.Collections.Generic;
using UnityEngine;

public class MenuNavigationController  {

    public GameObject CurrentMenuScreen { get; private set; }
    public GameObject CurrentPopup { get; private set; }

    private readonly List<GameObject> _menuScreens = new List<GameObject>();
    private readonly Transform _menuRootTransform;

    public MenuNavigationController(Transform menuRootTransform)
    {
        _menuRootTransform = menuRootTransform;
    }

    public void PushMenuScreen(GameObject menuScreen)
    {
        if (CurrentMenuScreen != null)
            CurrentMenuScreen.gameObject.SetActive(false);

        menuScreen.transform.SetParent(_menuRootTransform, false);
        menuScreen.gameObject.SetActive(true);
        _menuScreens.Add(menuScreen);
        CurrentMenuScreen = menuScreen;
    }

    public void PopMenuScreen()
    {
        if (_menuScreens.Count < 2) return;
        _menuScreens.Remove(CurrentMenuScreen);
        Object.Destroy(CurrentMenuScreen.gameObject);
        CurrentMenuScreen = _menuScreens[_menuScreens.Count - 1];
        CurrentMenuScreen.gameObject.SetActive(true);
    }

    public void ShowPopup(GameObject popup)
    {
        if (CurrentPopup != null)
            HidePopup();
        if (CurrentMenuScreen != null)
            CurrentMenuScreen.gameObject.SetActive(false);

        popup.transform.SetParent(_menuRootTransform, false);
        popup.gameObject.SetActive(true);
        CurrentPopup = popup;
    }

    public void HidePopup()
    {
        if (CurrentPopup == null) return;
        Object.Destroy(CurrentPopup.gameObject);
        CurrentPopup = null;
        CurrentMenuScreen.gameObject.SetActive(true);
    }

    public bool IsVisible
    {
        get { return _menuRootTransform.gameObject.activeSelf; }
        set { _menuRootTransform.gameObject.SetActive(value); }
    }

    public void OnAppButtonPressed()
    {
        if (CurrentPopup == null)
            PopMenuScreen();
    }
}