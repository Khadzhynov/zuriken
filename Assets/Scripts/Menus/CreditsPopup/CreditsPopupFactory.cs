﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsPopupFactory {

    private readonly CreditsPopup _creditsPopup;
    private readonly MenuNavigationController _menuNavigationController;

    public CreditsPopupFactory(CreditsPopup creditsPopup, MenuNavigationController menuNavigationController) {
        _creditsPopup = creditsPopup;
        _menuNavigationController = menuNavigationController;
    }

    public CreditsPopup Create() {
        var creditsPopup = Object.Instantiate(_creditsPopup);
        creditsPopup.Init(_menuNavigationController);
        return creditsPopup;
    }
}
