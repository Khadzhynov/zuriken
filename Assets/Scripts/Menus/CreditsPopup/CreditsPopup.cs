﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreditsPopup : MonoBehaviour {

    [SerializeField] private Button _backButton;

    private MenuNavigationController _menuNavigationController;

    private void Awake() {
        _backButton.onClick.AddListener(BackButtonPressed);
    }

    public void Init(MenuNavigationController menuNavigationController) {
        _menuNavigationController = menuNavigationController;
    }

    private void BackButtonPressed() {
        _menuNavigationController.PopMenuScreen();
    }

    private void OnDestroy() {
        _backButton.onClick.RemoveAllListeners();
    }
}
