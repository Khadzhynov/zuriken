﻿#define VIVE

using UnityEngine;
using UnityEngine.UI;

public class ScoreView : MonoBehaviour {

    [SerializeField]
    private Text _scoreText;

    private Score _score;

#if VIVE
    private static Score _scoreInstance;
#endif

    public void Init(Score score)
    {
        _score = score;
        _score.OnValueChanged += OnScoreChanged;
#if VIVE
        _scoreInstance = _score;
#endif
    }

    public static Score GetScore()
    {
        return _scoreInstance;
    }

    private void OnScoreChanged(int value)
    {
        _scoreText.text = value.ToString();
    }
}
