﻿using UnityEngine;

public class SettingsMenuFactory
{
    private readonly SettingsMenu _settingsMenuPrefab;
    private readonly Settings _settings;
    private IStartTutorialHandler _handler;
    private readonly MenuNavigationController _menuNavigationController;
    private readonly CreditsPopupFactory _creditsPopupFactory;

    public SettingsMenuFactory(SettingsMenu settingsMenuPrefab, Settings settings, MenuNavigationController menuNavigationController, CreditsPopupFactory creditsPopupFactory)
    {
        _settingsMenuPrefab = settingsMenuPrefab;
        _settings = settings;
        _menuNavigationController = menuNavigationController;
        _creditsPopupFactory = creditsPopupFactory;
    }

    public void SetStartTutorialHandler(IStartTutorialHandler handler)
    {
        _handler = handler;
    }

    public SettingsMenu Create()
    {
        var settingsMenu = Object.Instantiate(_settingsMenuPrefab);
        settingsMenu.Init(_settings, _menuNavigationController, _creditsPopupFactory);
        if (_handler != null)
            settingsMenu.SetStartTutorialHandler(_handler);
        return settingsMenu;
    }
}
