﻿using UnityEngine;
using UnityEngine.UI;

public interface IStartTutorialHandler
{
    void StartTutorial();
}

public class SettingsMenu : MonoBehaviour
{
    [SerializeField]
    private Slider _soundSlider;

    [SerializeField]
    private Button _startTutorialButton;

    [SerializeField] private Button _creditsButton;
    [SerializeField] private Button _backToMenuButton;

    private Settings _settings;
    private IStartTutorialHandler _startTutorialHandler;
    private MenuNavigationController _menuNavigationController;
    private CreditsPopupFactory _creditsPopupFactory;

    private void Awake()
    {
        _soundSlider.onValueChanged.AddListener(OnSoundVolumeChanged);
        _startTutorialButton.onClick.AddListener(OnStartTutorialPressed);
        _creditsButton.onClick.AddListener(OnCreditsButtonPressed);
        _backToMenuButton.onClick.AddListener(OnBackToMenuPressed);
    }

    public void Init(Settings settings, MenuNavigationController menuNavigationController, CreditsPopupFactory creditsPopupFactory)
    {
        _settings = settings;
        _soundSlider.value = _settings.SoundVolume;
        _menuNavigationController = menuNavigationController;
        _creditsPopupFactory = creditsPopupFactory;
    }

    public void SetStartTutorialHandler(IStartTutorialHandler handler)
    {
        _startTutorialHandler = handler;
    }

    private void OnSoundVolumeChanged(float volume)
    {
        _settings.SetSoundVolume(volume);
    }

    private void OnStartTutorialPressed()
    {
        if (_startTutorialHandler != null)
            _startTutorialHandler.StartTutorial();
    }

    private void OnCreditsButtonPressed() {
        var creditsMenu = _creditsPopupFactory.Create();
        _menuNavigationController.PushMenuScreen(creditsMenu.gameObject);
    }

    private void OnBackToMenuPressed() {
        _menuNavigationController.PopMenuScreen();
    }
}
