﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowableCollision : WeaponCollisionBase
{
    [SerializeField]
    private TrailRenderer _trail;

    [SerializeField]
    private float _maxLifeTime = 6f;
    private float _currentLifeTime;

    [SerializeField]
    private int _maxCollisionsCount = 5;
    private int _collisionsCount = 0;

    [SerializeField]
    private float _minDamageVelocity = 1f;

    [SerializeField]
    private Vector3 _ThrowRotationPlane = Vector3.up;

    private bool _collided;

    [SerializeField]
    private bool _destroyOnImpact = false;

    protected override void Awake()
    {
        SinglePlayerController.OnGameReset += GameResetHandler;
    }

    private void OnDestroy()
    {
        SinglePlayerController.OnGameReset -= GameResetHandler;
    }

    private void GameResetHandler(object sender, System.EventArgs e)
    {
        Destroy(gameObject);
    }

    public void StartFly(float throwSpeed)
    {
        if (_trail != null)
        {
            _trail.enabled = true;
        }

        throwSpeed = Mathf.InverseLerp(0, 3, throwSpeed);

        //_rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;

        if (_maxLifeTime > 0)
        {
            Destroy(gameObject, _maxLifeTime);
        }

        StartCoroutine(SpinShuriken(throwSpeed));
    }

    IEnumerator SpinShuriken(float throwSpeed)
    {
        yield return new WaitForEndOfFrame();
        if (_rigidbody != null)
        {
            _rigidbody.AddRelativeTorque(_ThrowRotationPlane * -10 * throwSpeed);
        }
    }

    public override void OnImpact()
    {
        if (_destroyOnImpact)
        {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        //_rigidbody.constraints = RigidbodyConstraints.None;

        if (_collided)
        {
            return;
        }

        if (_rigidbody != null)
        {

            if (_rigidbody.velocity.magnitude > _minDamageVelocity)
            {
                if (collision.collider.tag == "Head")
                {
                    //Debug.Log("hit head");
                    _collided = true;
                    Destroy(gameObject);
                }
                else if (collision.collider.tag == "Zombie")
                {
                    //Debug.Log("hit zombie body");
                    _collided = true;
                    Destroy(gameObject);
                }
            }
        }

        if (_maxCollisionsCount > 0)
        {
            _collisionsCount++;


            if (_collisionsCount >= _maxCollisionsCount)
            {
                Destroy(gameObject);
            }
        }

    }
}
