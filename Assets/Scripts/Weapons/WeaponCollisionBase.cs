﻿using UnityEngine;
using System.Collections;
using System;


public class WeaponCollisionBase : MonoBehaviour
{
    protected Rigidbody _rigidbody;

    [SerializeField]
    private float scoreMultipler;

    [SerializeField]
    protected float damageMultipler;

    protected virtual void Awake()
    {
        _rigidbody = gameObject.GetComponent<Rigidbody>();
    }

    public virtual void OnImpact()
    {

    }

    public virtual float GetDamageMultipler()
    {
        return damageMultipler;
    }

    public virtual float GetDamage(Collision collision)
    {
        //return damageMultipler * collision.relativeVelocity.magnitude;
        return damageMultipler * collision.rigidbody.velocity.magnitude;
    }

    public virtual float GetScoreMultipler()
    {
        return scoreMultipler;
    }
}
