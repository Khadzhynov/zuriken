﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class MeeleCollision : WeaponCollisionBase
{
    [SerializeField]
    private Hand.HoldedObjectSize _sizeInHand;

    private VRTK_InteractableObject _interactable;

    private Vector3 _previousPosition;

    private float _speed;

    private bool _thrown = false;

    Vector3 _initialPosition;

    Quaternion _initialRotation;

    protected override void Awake()
    {
        base.Awake();
        _interactable = gameObject.GetComponent<VRTK_InteractableObject>();
        _interactable.InteractableObjectUngrabbed += HandleUngrabbed;
        _interactable.InteractableObjectGrabbed += HandleGrabbed;

        SinglePlayerController.OnGameReset += GameResetHandler;

        _initialPosition = transform.position;
        _initialRotation = transform.rotation;
    }

    private void GameResetHandler(object sender, System.EventArgs e)
    {
        //Debug.Log("Game reset: from " + transform.position + " to " + _initialPosition);
        transform.position = _initialPosition;
        transform.rotation = _initialRotation;
        _rigidbody.velocity = Vector3.zero;
    }

    private void OnDestroy()
    {
        _interactable.InteractableObjectUngrabbed -= HandleUngrabbed;
        SinglePlayerController.OnGameReset -= GameResetHandler;
    }

    private void HandleUngrabbed(object sender, InteractableObjectEventArgs e)
    {
        Hand.Release(e.interactingObject.GetComponent<TrowableSpawper>().HandSide);
        _thrown = true;
    }

    private void HandleGrabbed(object sender, InteractableObjectEventArgs e)
    {
        Hand.Hold(e.interactingObject.GetComponent<TrowableSpawper>().HandSide, _sizeInHand);
    }

    private void Update()
    {
        if (_interactable != null && _interactable.IsGrabbed())
        {
            _speed = Vector3.Distance(_previousPosition, transform.position) * Time.deltaTime;
            _previousPosition = transform.position;
        }

        if (_rigidbody.IsSleeping())
        {
            _thrown = false;
        }

    }

    public override float GetDamage(Collision collision)
    {
        if (_interactable != null && _interactable.IsGrabbed())
        {
            //----- Meele attack
            return damageMultipler * _speed * 10000;
        }
        else
        {
            //----- Distant attack
            if (_thrown)
            {
                _thrown = false;
                return damageMultipler * collision.rigidbody.velocity.magnitude;
            }
            else
            {
                return 0;
            }
        }
    }

}
