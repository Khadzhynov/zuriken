﻿#define VIVE
using UnityEngine;
using System.Collections;
using System;

public class SurikenCollision : MonoBehaviour
{
    private bool _collided;

    [SerializeField]
    private TrailRenderer _trail;

    [SerializeField]
    private float _maxLifeTime = 6f;

    private float _currentLifeTime;

    [SerializeField]
    private int _maxCollisionsCount = 5;

    private int _collisionsCount = 0;

    [SerializeField]
    private float _stuckChancePercent = 10;

    [SerializeField]
    private float _minDamageSpeed = 1f;

    private Score _score;

    private Rigidbody _rigidbody;

    public void Init(Score score)
    {
        _score = score;
    }

#if VIVE

    private void Awake()
    {
        _score = ScoreView.GetScore();
        _rigidbody = gameObject.GetComponent<Rigidbody>();
        SinglePlayerController.OnGameReset += GameResetHandler;
    }

    private void OnDestroy()
    {
        SinglePlayerController.OnGameReset -= GameResetHandler;
    }

    private void GameResetHandler(object sender, System.EventArgs e)
    {
        Destroy(gameObject);
    }

    public void StartFly(float throwSpeed)
    {
        if (_trail != null)
        {
            _trail.enabled = true;
        }

        throwSpeed = Mathf.InverseLerp(0, 3, throwSpeed);

        _rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;

        Destroy(gameObject, _maxLifeTime);

        StartCoroutine(SpinShuriken(throwSpeed));
    }

    IEnumerator SpinShuriken(float throwSpeed)
    {
        yield return new WaitForEndOfFrame();
        _rigidbody.AddRelativeTorque(Vector3.up * -10 * throwSpeed);
    }

#endif

    void OnCollisionEnter(Collision collision)
    {
        _collisionsCount++;

        _rigidbody.constraints = RigidbodyConstraints.None;

        if (_collided) {
            return;
        }

        if (_rigidbody.velocity.magnitude > _minDamageSpeed)
        {

            if (collision.collider.tag == "Head")
            {
                //Debug.Log("hit head");
                _collided = true;
                Destroy(gameObject);
                _score.AddScore(10);
            }
            else if (collision.collider.tag == "Zombie")
            {
                //Debug.Log("hit zombie body");
                _collided = true;
                Destroy(gameObject);
                _score.AddScore(5);
            }
        }

        /*
        if ( _stuckChancePercent > UnityEngine.Random.Range(0, 100))
        {
            _rigidbody.velocity = Vector3.zero;
            _rigidbody.isKinematic = true;
            transform.parent = collision.transform;
        }
        */


        if (_collisionsCount >= _maxCollisionsCount)
        {
            Destroy(gameObject);
        }

    }

    public float GetDamageMultipler()
    {
        return Mathf.Clamp01( Mathf.Round( _rigidbody.velocity.magnitude ) );
    }

    public float GetDamage(float relativeVelocity, float relativeAngularVelocity)
    {
        throw new NotImplementedException();
    }

    public float GetScoreMultipler()
    {
        throw new NotImplementedException();
    }
}
