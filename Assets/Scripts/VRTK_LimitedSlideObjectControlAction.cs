﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class VRTK_LimitedSlideObjectControlAction : VRTK_SlideObjectControlAction
{
    [SerializeField]
    LayerMask _raycastAgainst;

    [SerializeField]
    float _raycastDistance = 1f;

    [SerializeField]
    float _bodyRadius = 0.5f;

    protected override void Move(GameObject controlledGameObject, Transform directionDevice, Vector3 axisDirection)
    {
        if (directionDevice && directionDevice.gameObject.activeInHierarchy && controlledGameObject.activeInHierarchy)
        {
            float storeYPosition = controlledGameObject.transform.position.y;
            Vector3 updatedPosition = axisDirection * currentSpeed * Time.deltaTime;
            Vector3 finalPosition = controlledGameObject.transform.position + updatedPosition;
            finalPosition = new Vector3(finalPosition.x, storeYPosition, finalPosition.z);

            Ray ray = new Ray(controlledGameObject.transform.position, finalPosition - controlledGameObject.transform.position);


            //if (Physics.Raycast(ray, _raycastDistance, _raycastAgainst) == false)
            if (Physics.SphereCast(ray, _bodyRadius, _raycastDistance, _raycastAgainst) == false)
            {
                if (CanMove(bodyPhysics, controlledGameObject.transform.position, finalPosition))
                {
                    controlledGameObject.transform.position = finalPosition;
                }
            }
        }
    }

}
