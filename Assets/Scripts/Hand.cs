﻿using UnityEngine;
using VRTK;

public class Hand : MonoBehaviour
{
    public enum HandSide { Right, Left }

    public enum HoldedObjectSize { HoldSmall, HoldBig, HoldShuriken }

    [SerializeField]
    private Transform _placeForShuriken;

    [SerializeField]
    private HandSide _hand;

    public Transform PlaceForShuriken { get { return _placeForShuriken; } }
    public Animator Animator { get; private set; }

    private static Transform _shurikenPlace;

    private static Hand _RightHandInstance;
    private static Hand _LeftHandInstance;

    void Awake()
    {
        Animator = GetComponentInChildren<Animator>();
        
        if (_hand == HandSide.Left)
        {
            _LeftHandInstance = this;
        }
        else
        {
            _RightHandInstance = this;
        }

        SinglePlayerController.OnGameReset += GameResetHandler;

    }

    private void OnDestroy()
    {
        SinglePlayerController.OnGameReset -= GameResetHandler;
    }

    private void GameResetHandler(object sender, System.EventArgs e)
    {
        Animator.SetTrigger("Idle");
    }

    public static Transform GetSurikenPlace()
    {
        return _shurikenPlace;
    }

    public static void Hold ( HandSide hand , HoldedObjectSize size)
    {
        if (hand == HandSide.Left)
        {
            _LeftHandInstance.Animator.SetTrigger(size.ToString());
        }
        else
        {
            _RightHandInstance.Animator.SetTrigger(size.ToString());
        }
    }

    public static void ThrowShuriken(HandSide hand)
    {
        if (hand == HandSide.Left)
        {
            _LeftHandInstance.Animator.SetTrigger("Throw");
        }
        else
        {
            _RightHandInstance.Animator.SetTrigger("Throw");
        }
    }

    public static void Release(HandSide hand)
    {
        if (hand == HandSide.Left)
        {
            _LeftHandInstance.Animator.SetTrigger("Idle");
        }
        else
        {
            _RightHandInstance.Animator.SetTrigger("Idle");
        }
    }
}
