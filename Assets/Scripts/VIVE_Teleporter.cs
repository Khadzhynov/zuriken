﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class VIVE_Teleporter : MonoBehaviour
{
    [SerializeField]
    VRTK_BasicTeleport _teleporter;

    [SerializeField]
    public Transform MenuPosition;

    [SerializeField]
    public Transform GamePosition;

    public static VIVE_Teleporter Instance;

    Vector3 _cachedPosition;

    Quaternion _cachedRotation;

    private void Awake()
    {
        Instance = this;
    }

    public static void StartMenu()
    {
        Instance._cachedPosition = Instance.GamePosition.position;

        Instance._cachedRotation = Instance.GamePosition.rotation;

        Instance._teleporter.ForceTeleport(Instance.MenuPosition.position, Instance.MenuPosition.rotation);
    }

    public static void GoToMenu(Vector3 position, Quaternion rotation)
    {
        Instance._cachedPosition = position;

        Instance._cachedRotation = rotation;

        Instance._teleporter.ForceTeleport(Instance.MenuPosition.position, Instance.MenuPosition.rotation);
    }

    public static void GoToGameplay()
    {
        Instance._teleporter.ForceTeleport(Instance.GamePosition.position, Instance.GamePosition.rotation);
    }

    public static void GoToCachedPosition()
    {
        Instance._teleporter.ForceTeleport(Instance._cachedPosition, Instance._cachedRotation);
    }

}
