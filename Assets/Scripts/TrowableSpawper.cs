﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

/// <summary>
/// Spawns throwable weapons to be autograbbed in a hand
/// </summary>
public class TrowableSpawper : MonoBehaviour
{
    [SerializeField]
    private Hand.HandSide _hand;

    [SerializeField]
    private VRTK_InteractGrab _Grabber;

    [SerializeField]
    private VRTK_InteractTouch _Toucher;

    [SerializeField]
    private GvrAudioSource _audioSource;

    [SerializeField]
    private VRTK_ControllerEvents.ButtonAlias _spawnButton = VRTK_ControllerEvents.ButtonAlias.TriggerPress;

    public static event System.EventHandler OnShurikenSpawned;

    public static event System.EventHandler OnShurikenThrown;


    private Vector3 _previousPosition;

    private float velocity;

    [SerializeField]
    VRTK_InteractableObject _customShurikenPrefab;

    public Hand.HandSide HandSide
    {
        get
        {
            return _hand;
        }

        set
        {
            _hand = value;
        }
    }

    private void Awake()
    {
        gameObject.GetComponent<VRTK_ControllerEvents>().SubscribeToButtonAliasEvent(_spawnButton, true, SpawnShuriken);

        gameObject.GetComponent<VRTK_ControllerEvents>().SubscribeToButtonAliasEvent(_spawnButton, false, ThrowOnReleased);

        _Grabber = gameObject.GetComponent<VRTK_InteractGrab>();
        _Toucher = gameObject.GetComponent<VRTK_InteractTouch>();

        StartCoroutine(CalcVelocity());
    }

    IEnumerator CalcVelocity()
    {
        while (Application.isPlaying)
        {
            _previousPosition = transform.position;
            yield return new WaitForEndOfFrame();

            velocity = (_previousPosition - transform.position).magnitude / Time.deltaTime;
        }
    }

    private void ThrowOnReleased(object sender, ControllerInteractionEventArgs e)
    {
        ThrowShuriken(null, default(ObjectInteractEventArgs));
    }

    private void OnDestroy()
    {
        gameObject.GetComponent<VRTK_ControllerEvents>().UnsubscribeToButtonAliasEvent(_spawnButton, true, SpawnShuriken);

        gameObject.GetComponent<VRTK_ControllerEvents>().UnsubscribeToButtonAliasEvent(_spawnButton, false, ThrowOnReleased);
    }

    private void SpawnShuriken(object sender, ControllerInteractionEventArgs e)
    {
        if (Time.timeScale != 0)
        {
            StartCoroutine(CreateShuriken());
        }
    }

    private void ThrowShuriken(object sender, ObjectInteractEventArgs e)
    {
        if (Time.timeScale != 0)
        {
            _audioSource.Play();

            _Grabber.GetGrabbedObject().GetComponent<ThrowableCollision>().StartFly(velocity);

            _Grabber.ForceRelease(true);

            if (OnShurikenThrown != null)
            {
                OnShurikenThrown(null, null);
            }

            global::Hand.ThrowShuriken(HandSide);
        }
    }

    IEnumerator CreateShuriken()
    {
        yield return new WaitForEndOfFrame();

        VRTK_InteractableObject grabbableObject = Instantiate(_customShurikenPrefab.gameObject).GetComponent<VRTK_InteractableObject>();

        //grabbableObject.transform.position = transform.position;
        //grabbableObject.transform.rotation = transform.rotation;


        grabbableObject.transform.position = _Grabber.controllerAttachPoint.transform.position; 
        grabbableObject.transform.rotation = _Grabber.controllerAttachPoint.transform.rotation;





        _Toucher.ForceStopTouching();
        _Toucher.ForceTouch(grabbableObject.gameObject);
        _Grabber.AttemptGrab();

        Hand.Hold(HandSide, Hand.HoldedObjectSize.HoldShuriken);
    }
}
