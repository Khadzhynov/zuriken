﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VIVE_AddCollidersToUIButtons : MonoBehaviour
{
    private void Start()
    {
        Button[] buttons = gameObject.GetComponentsInChildren<Button>();

        for (int i = 0; i < buttons.Length; i++)
        {
            var collider = buttons[i].gameObject.GetComponent<BoxCollider>();
            if (collider == null)
            {
                collider = buttons[i].gameObject.AddComponent<BoxCollider>();
            }

            collider.center = buttons[i].image.sprite.bounds.center;
            collider.size = new Vector3( (buttons[i].transform as RectTransform).sizeDelta.x, (buttons[i].transform as RectTransform).sizeDelta.y, 1f);
        }
    }
}
